from vhqueue.VHQueue import VHQueue
from BinarySearchTree.BSTreeBalancer import BSTreeBalancer
from BinarySearchTree.AvlNode import AvlNode

def defaultTraverseAction(x):
    if x: print(x._root)


class BinarySearchTree:
    def __init__(self, row=None, left=None, right=None, parent=None,nodeType=AvlNode):
        self._root = None
        self._balancer = BSTreeBalancer(self)
        if row: self._root = nodeType(row,left,right,parent,balancer=self._balancer)
        self._maxNodes = 0
        self._nodeType = nodeType

    def addItem(self, row):
        self._maxNodes = self._maxNodes + 1
        if self._root:
            self._root.addItem(row)
            return True
        self._root = self._nodeType(row,balancer=self._balancer)
        return True

    def getKey(self):
        if self._root: return self._root.getKey()
        return None

    def resetSmartSearch(self):
        pass

    def sort(self):
        return self.criarAlturaMinima()

    def getValue(self):
        return self._root.getValue()

    def getItem(self, key, condition = None):
        if not self._root: return False
        return self._root.getItem(key)

    def getAllItens(self):
        return [item for item in self]

    def getItemWhere(self,column,key,isFirstKey = None):
        return [item for item in self if getattr(item,column) == key]

    def preOrderTraversal(self, action=defaultTraverseAction):
        if not self._root: return False
        self._root.preOrderTraversal(action)

    def inOrderTraversal(self, action=defaultTraverseAction):
        if not self._root: return False
        self._root.inOrderTraversal(action)

    def postOrderTraversal(self, action=defaultTraverseAction):
        if not self._root: return False
        self._root.postOrderTraversal(action)

    def breadthFirstTraversal(self, action = defaultTraverseAction,printing = False):
        queue = VHQueue()
        root = self
        queue.push(root)
        while(queue.size > 0):
            root = queue.remove()
            print(root)
            if root:
                action(root)
                if printing: print("\n")
                queue.push(root._left)
                queue.push(root._right)

    def rotacionaDir(self, node):
        return self._balancer.rotacionaDir(node)

    def changeParentsDir(self,node,promotedNode):
        return self._balancer.changeParentsDir(node, promotedNode)

    def changeParentsEsq(self,node,promotedNode):
        return self._balancer.changeParentsEsq(node,promotedNode)

    def rotacionaEsq(self, node):
        return self._balancer.rotacionaEsq(node)

    def criarEspinhaDorsal(self):
        return self._balancer.criarEspinhaDorsal()

    def criarAlturaMinima(self):
        return self._balancer.criarAlturaMinima()

    def isEspinhaDorsal(self):
        tmp = self._root
        while tmp != None:
            if tmp._left != None:
                return False
            tmp = tmp._right
        return True

    def getHeight(self,node = "root"):
        if node == "root": node = self._root
        if node is None:
            return 0
        else:
            return max(self.getHeight(node._left), self.getHeight(node._right)) + 1

    def __repr__(self):
        return str(self._root.getKey())

    def __iter__(self):
        queue = VHQueue()
        head = self._root
        queue.push(head)
        while head is not None:
            head = queue.remove()
            if head:
                yield head.getValue()
                if head._left: queue.push(head._left)
                if head._right: queue.push(head._right)
        return
import math
import BinarySearchTree.BinarySearchTree
from BinarySearchTree.TreeNode import TreeNode
class BSTreeBalancer:
    def __init__(self, tree):
        self.btree = tree
        self.maxNodes = 0

    def rotacionaDir(self, node):
        promotedNode = node._left
        node._left = promotedNode._right
        if promotedNode._right == None:
            node._left = None

        promotedNode._right = node
        self.btree.changeParentsDir(node,promotedNode)
        if self.btree._root == node:
            self.btree._root = promotedNode
        return promotedNode

    def changeParentsDir(self,node,promotedNode):
        if node._parent != None:
            if node._parent._right == node: node._parent._right = promotedNode
            else: node._parent._left = promotedNode
        promotedNode._parent = node._parent
        node._parent = promotedNode
        if node._left != None: node._left._parent = node

    def changeParentsEsq(self,node,promotedNode):
        if node._parent != None:
            if node._parent._right == node: node._parent._right = promotedNode
            else: node._parent._left = promotedNode
        promotedNode._parent = node._parent
        node._parent = promotedNode
        if node._right != None: node._right._parent = node

    def rotacionaEsq(self, node):
        promotedNode = node._right
        node._right = promotedNode._left
        if promotedNode._left == None:
            node._right = None

        promotedNode._left = node
        self.btree.changeParentsEsq(node, promotedNode)
        if self.btree._root == node:
            self.btree._root = promotedNode
        return promotedNode

    def criarEspinhaDorsal(self):
        treeRoot = self.btree._root
        tmp = self.btree._root
        while (tmp != None):
            if tmp._left != None:
                tmp = self.rotacionaDir(tmp)
                if treeRoot == tmp._right:
                    treeRoot = tmp
            else:
                tmp = tmp._right
        return treeRoot

    def criarAlturaMinima(self):
        self.btree.criarEspinhaDorsal()
        n = self.btree._maxNodes
        m = math.trunc(math.pow(2,math.trunc(math.log(n+1,2)))-1)
        treeRoot = self.btree._root
        initRotations = n - m
        tmp = self.btree._root
        for i in range(0, initRotations):
            tmp = self.btree.rotacionaEsq(tmp)
            if treeRoot == tmp._left:
                treeRoot = tmp
            tmp = tmp._right
        while (m > 1):
            tmp = treeRoot
            m = math.trunc(m / 2)
            for i in range(0, m):
                if tmp == None or tmp._right == None: continue
                tmp = self.btree.rotacionaEsq(tmp)
                if treeRoot == tmp._left:
                    treeRoot = tmp
                tmp = tmp._right
        self.btree._root = treeRoot
        return self.btree

class AvlNode:
    def __init__(self, row=None, left=None, right=None, parent=None, balancer = None):
        self._value = row
        self._left = left
        self._right = right
        self._parent = parent
        self._balance = 0
        self._balancer = balancer

    def addItem(self, row):
        if self._value == row:
            return False

        predecessors = self.getPredecessors(row.getKey())
        parent = predecessors[-1]
        newTree = AvlNode(row,parent=parent,balancer=self._balancer)
        if row > parent:
            parent._right = newTree
        else:
            parent._left = newTree

        for predecessor in reversed(predecessors):
            predecessor.updateBalance(newTree)
            if predecessor.rebalance(): break
        return True

    def updateBalance(self,newNode):
        if newNode > self:
            self._balance += 1
            return
        self._balance -= 1

    def getPredecessors(self, key):
        tmp = self
        previousRoot = []
        while (tmp != None):
            previousRoot.append(tmp)
            tmpKey = tmp.getKey()
            if key == tmpKey:
                return tmp.getValue()
            if (key > tmpKey):
                tmp = tmp._right
                continue
            tmp = tmp._left
        return previousRoot

    def rebalance(self):
        if self._balance > 1:
            if self._right._balance == -1:
                self._balancer.rotacionaDir(self._right)
            parent = self._balancer.rotacionaEsq(self)
        elif self._balance < -1:
            if self._left._balance == 1:
                self._balancer.rotacionaEsq(self._left)
            parent = self._balancer.rotacionaDir(self)
        else: return False
        parent.calcBalance()
        self.calcBalance()
        if self._left: self._left.calcBalance()
        if self._right: self._right.calcBalance()
        return True



    def calcBalance(self):
        if self._right and self._left:
            self._balance = -self._left.getHeight() + self._right.getHeight()
            return
        if not self._right and not self._left:
            self._balance = 0
            return
        if self._right:
            self._balance = self._right.getHeight() + 1
            return
        if self._left:
            self._balance = (self._left.getHeight() + 1) * -1
            return


    def getHeight(self):
        if self._left and self._right:
            return max(self._left.getHeight(), self._right.getHeight()) + 1
        elif self._left:
            return self._left.getHeight() + 1
        elif self._right:
            return self._right.getHeight() + 1
        return 0

    def getItem(self, key, condition = None,inserting = False):
        tmp = self
        previousRoot = tmp
        while (tmp != None):
            previousRoot = tmp
            tmpKey = tmp.getKey()
            if key == tmpKey:
                if not condition or tmp.getKey(condition.field) == condition.key:
                    return tmp.getValue()
            if (key > tmpKey):
                tmp = tmp._right
                continue
            tmp = tmp._left
        if inserting: return previousRoot
        return False

    def getKey(self):
        if self._value: return self._value.getKey()
        return None

    def getValue(self):
        return self._value.getValue()

    def preOrderTraversal(self, action):
        action(self._value)
        if self._left: self._left.preOrderTraversal(action)
        if self._right: self._right.preOrderTraversal(action)

    def inOrderTraversal(self, action):
        if self._left: self._left.inOrderTraversal(action)
        action(self._value)
        if self._right: self._right.inOrderTraversal(action)

    def postOrderTraversal(self, action):
        if self._left: self._left.postOrderTraversal(action)
        if self._right: self._right.postOrderTraversal(action)
        action(self._value)

    def __repr__(self):
        return str(self._value)

    def __gt__(self, other):
        return self.getKey() > other.getKey()
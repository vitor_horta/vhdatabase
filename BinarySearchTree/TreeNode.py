class TreeNode:
    def __init__(self, row=None, left=None, right=None, parent=None, balancer = None ):
        self._value = row
        self._left = left
        self._right = right
        self._parent = parent
        self._maxNodes = 0

    def addItem(self, row):
        self._maxNodes = self._maxNodes + 1
        if self._value == row:
            return False

        parent = self.getItem(row.getKey())
        newTree = TreeNode(row,parent=parent)
        if row > parent:
            parent._right = newTree
            return True

        parent._left = newTree
        return True

    def getItem(self, key, condition = None):
        tmp = self
        previousRoot = tmp
        while (tmp != None):
            previousRoot = tmp
            tmpKey = tmp.getKey()
            if key == tmpKey:
                if not condition or tmp.getKey(condition.field) == condition.key:
                    return tmp.getValue()
            if (key > tmpKey):
                tmp = tmp._right
                continue
            tmp = tmp._left
        return previousRoot

    def getKey(self):
        if self._value: return self._value.getKey()
        return None

    def getValue(self):
        return self._value.getValue()

    def preOrderTraversal(self, action):
        action(self._value)
        if self._left: self._left.preOrderTraversal(action)
        if self._right: self._right.preOrderTraversal(action)

    def inOrderTraversal(self, action):
        if self._left: self._left.inOrderTraversal(action)
        action(self._value)
        if self._right: self._right.inOrderTraversal(action)

    def postOrderTraversal(self, action):
        if self._left: self._left.postOrderTraversal(action)
        if self._right: self._right.postOrderTraversal(action)
        action(self._value)

    def __repr__(self):
        return str(self._value)
from node.Node import Node

class LinkedList:
    head = None

    def __init__(self):
        self.size = 0

    def addItem(self,row):
        self.size += 1
        if(self.head == None):
            self.head = Node(row.getKey(),row)
            return True
        self.head = Node(row.getKey(),row,self.head)
        return True

    def getAllItens(self):
        nodes = []
        node = self.head
        while (node != None):
            nodes.append(node.getValue())
            node = node.next
        return nodes

    def removeItem(self,key):
        node = self.head
        previousNode = None
        while (node != None):
            if node.key == key:
                previousNode.next = node.next
                node.next = None
                del node
                self.size -= 1
                return True
            previousNode = node
            node = node.next
        return False

    def __len__(self):
        return self.size

    def __str__(self):
        nodes = []
        node = self.head
        while(node != None):
            nodes.append(node.getValue())
            node = node.next
        return str(nodes)

    def sort(self):
        return self

    def getItemWhere(self,column,key,isFirstPkey = None):
        node = self.head
        itens = []
        while (node != None):
            if node.value.getKey(column) == key:
                itens.append(node.getValue())
            node = node.next
        return itens

    def resetSmartSearch(self):
        pass

    def getItem(self,key, condition = None):
        node = self.head
        while (node != None):
            if node.key == key:
                if not condition or node.getKey(condition.field) == condition.key:
                    return node.getValue()
            node = node.next
        return False

    def remove(self,position):
        value = self.head.getValue()
        self.head = self.head.next
        return value

    def __getitem__(self, position):
        item = self.head
        for i in range(0,position):
            if item == None:
                return -1
            item = item.next

        if item == None:
            raise StopIteration

        return item.getValue()

    def __iter__(self):
        initialHead = self.head
        head = self.head
        while head is not None:
            i = head
            val = yield i if not hasattr(i,'getValue') else i.getValue()
            if val == "restart":
                head = initialHead
            elif val == "mark":
                initialHead = head
            else:
                head = head.next
        return
from linkedList.LinkedList import LinkedList
from vhskiplist.slnode.SLNode import SLNode
from random import randint

class VHSkipList:
    _lists = []

    def __init__(self):
        self._lists = []
        self.size = 0
        self.matchNode = None
        self.lastNode = None

    '''
    Sorteia nivel do item a ser inserido
    '''
    def getItemLevel(self):
        level = 0
        while bool((randint(0, 1))):
            level += 1
        return level

    '''
    Chama sorteio de nivel do item e depois chama função de inserção
    Se a inserção for verdadeira, aumenta tamanho da skiplist em 1
    '''
    def addItem(self,data,itemLevel = None):
        if itemLevel == None:   itemLevel = self.getItemLevel()
        if self.insertItem(data,itemLevel):
            self.size += 1
            return True
        return False

    '''
    Insere novo item
    Se o nivel do item for maior que a quantidade de listas, cria novas listas colocando o item como inicial
    Procura os predecessores do item. (Ver get predecessor)
    Para cada predecessor, coloca o item como próximo
    Se o retorno de getPredecessors for false, desfaz as listas criadas anteriormente
    '''
    def insertItem(self,data,itemLevel,downNode = None,upNode = None,currentLevel = 0,insertedLists = []):
        if currentLevel > itemLevel: return
        if(itemLevel >= len(self._lists)):
            newData = data
            if currentLevel > 0: newData = data.getKey()
            head = self.createNewList(newData)
            head.downNode = downNode
            if upNode == None:
                upNode = head
            insertedLists.append(len(self._lists)-1)
            currentLevel += 1
            self.insertItem(data,itemLevel,head,upNode,currentLevel,insertedLists)

            return True

        predecessors = self.getPredecessors(data,itemLevel)
        if predecessors == False:
            for i in reversed(insertedLists):
                if i > 0:
                    del self._lists[i]
            return

        for level in list(reversed(sorted(predecessors.keys()))):
            newData = data
            if level > 0: newData = data.getKey()
            node = predecessors[level]
            newNode = SLNode(newData,None)
            if upNode: upNode.downNode = newNode
            upNode = newNode

            if newNode.getKey() > node.getKey():
                newNode.next = node.next
                node.next = newNode
                continue

            l = self._lists[level]

            if node == l.head:
                newNode.next = node
                l.head = newNode
        return True

    '''
    Retorna os predecessores do item a ser inserido
    Busca pelo predecessor do item. Se o nivel do item for igual ao  nivel do predecessor, adiciona o predecessor na lista
    Retorna depois de analisar a ultima lista (nivel 0)
    '''
    def getPredecessors(self,data,nodeLevel):
        if not isinstance(data, str): data = data.getKey()
        maxLevel = len(self._lists) - 1
        predecessors = {}
        list = self._lists[maxLevel]
        head = list.head

        while (head != None):
            if (head.getKey() == data):
                maxLevel -= 1
                if maxLevel == -1: return False
                list = self._lists[maxLevel]
                head = list.head
                continue
            if (head.next != None and data >= head.next.getKey()):
                head = head.next
                continue
            if (data > head.getKey()):
                if nodeLevel >= maxLevel: predecessors[maxLevel] = head
                head = head.downNode
                maxLevel -= 1
            else:
                if nodeLevel >= maxLevel: predecessors[maxLevel] = head
                maxLevel -= 1
                if maxLevel == -1: return predecessors
                list = self._lists[maxLevel]
                head = list.head
        return predecessors

    '''
    Atualiza tamanho da lista encadeada de nivel 0 que contem todos os itens e a retorna
    '''
    def getAllItens(self):
        self._lists[0].size = self.size
        return self._lists[0]

    def sort(self):
        return self

    '''
    Cria nova lista com item a ser inserido no inicio
    '''
    def createNewList(self,data):
        newList = LinkedList()
        newList.head = SLNode(data,None)
        self._lists.append(newList)
        return newList.head

    '''
    Retorna itens tal que itens[i].column == key
    Se a coluna passada for primeira chave, faz uma busca que aproveita desse index (Ver getWhereByPkey)
    '''
    def getItemWhere(self,column,key,isFirstKey = None):
        if isFirstKey:
            return self.getWhereByPkey(key, column=column)
        return self.getWhereByNonPkey(column,key)

    '''
    Passa e retorna todos os itens que satisfazem a condição
    '''
    def getWhereByNonPkey(self,column,key):
        return [row for row in self if (getattr(row, column) == key)]

    '''
    Passa pelos itens indexados e retorna os itens que satisfazem a condição
    Este método não precisa varrer todos os itens, já que ao encontrar um item com chave maior do que a recebida, o processamento pode parar
    Isso acontece pois a lista está ordenada pela coluna passada
    A variável matchNode indica onde a última busca teve seu primeiro sucesso
    A variável lastNode indica onde a última busca parou
    Se a chave recebida for maior que matchNode, a busca pode começar por lastNode
    Caso contrário, inicia-se por matchNode, que tem default no inicio
    '''
    def getWhereByPkey(self,key,condition = None, column = None):
        if not isinstance(key, str): key = key.getKey()
        itens = []
        if not self.matchNode: head = self._lists[0].head
        elif self.lastNode and key > self.matchNode.getKey(column): head = self.lastNode
        else: head = self.matchNode

        while (head != None):
            if (head.getKey(column) == key and head.checkCondition(condition)):
                if not self.matchNode or self.matchNode.getKey(column) != head.getKey(column): self.matchNode = head
                itens.append(head.getValue())
            if(head.getKey(column) > key):
                self.lastNode = head
                return itens
            head = head.next
        return itens

    '''
    Reseta parametros da busca getWhereByPKey
    '''
    def resetSmartSearch(self):
        self.matchNode = None
        self.lastNode = None

    '''
    Recebe um nó e encontra seu correspondente na lista inicial
    '''
    def getFirstLevelNode(self,node):
        while node.downNode != None:
            node = node.downNode
        return node

    '''
    Busca por item atráves da chave primária. O(log n)
    Se uma condição for passada, retorna apenas se o item satisfaz a condição
    '''
    def getItem(self,data,condition = None):
        if not isinstance(data, str): data = data.getKey()
        maxLevel = len(self._lists) - 1

        list = self._lists[maxLevel]
        while list.head.getKey() > data:
            maxLevel -= 1
            if maxLevel == -1:  return
            list = self._lists[maxLevel]
        head = list.head
        while (head != None):
            if (head.getKey() == data):
                return self.getDataFromNode(head,condition)
            if (head.next != None and data >= head.next.getKey()):
                head = head.next
            else:
                maxLevel -= 1
                head = head.downNode

    '''
    Busca pela chave primária e remove item
    '''
    def removeItem(self,data):
        if self.remove(data):
            self.size -= 1
            return True
        return False

    '''
    Busca pela chave primária e remove item
    '''
    def remove(self,data):
        maxLevel = len(self._lists) - 1
        removed = False
        l = self._lists[maxLevel]
        while l.head.getKey() > data:
            maxLevel -= 1
            if maxLevel == -1:
                return False
            l = self._lists[maxLevel]
        head = l.head
        while (head != None):
            if (head.getKey() == data):
                removed = True
                l.head = head.next
                if l.head == None:  del self._lists[maxLevel]
                maxLevel -= 1
                if maxLevel == -1:  return removed
                l = self._lists[maxLevel]
                head = l.head
                continue
            if head.next != None:
                if data > head.next.getKey():
                    head = head.next
                else:
                    removed = True
                    head.next = head.next.next
                    maxLevel -= 1
                    if maxLevel == -1:  return removed
                    l = self._lists[maxLevel]
                    head = head.downNode
            else:
                maxLevel -= 1
                if maxLevel == -1:  return removed
                l = self._lists[maxLevel]
                head = head.downNode

        return removed


    def getDataFromNode(self,node,condition):
        while node.downNode != None:
            node = node.downNode
        if node.checkCondition(condition): return node.getValue()
        return False

    def __iter__(self):
        return iter(self._lists[0])

    def __len__(self):
        return self.size
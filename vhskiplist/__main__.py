from skiplistold.SkipList import SkipList

skipList = SkipList()
skipList.addItem(10)
skipList.addItem(30)
skipList.addItem(45)
skipList.addItem(60)
data = skipList.getItem(45)
print(data)
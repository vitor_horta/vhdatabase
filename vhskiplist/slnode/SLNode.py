class SLNode:
    data = None
    next = None
    downNode = None
    def __init__(self,data = None, node = None):
        self._value = data
        self.next = node

    def getKey(self,column = None):
        if not isinstance(self._value, str):return self._value.getKey(column)
        if column: return self.getFirstColumn()
        return self._value

    def getValue(self):
        if not isinstance(self._value, str): return self._value.getValue()
        return self._value

    def getFirstColumn(self):
        return self._value.split('#')[1]
    def checkCondition(self,condition):
        return self._value.checkCondition(condition)

    def __repr__(self):
        return str(self._value)
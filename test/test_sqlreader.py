import unittest
from sqlreader.SqlReader import SqlReader

class TestSqlReader(unittest.TestCase):
    def setUp(self):
        self.reader = SqlReader()

    def testMakeRow(self):
        row = "D964  	E.H. Jr. Gruger, R.W. Nelson, M.E. Stansby	Fatty acid composition of oils from 21 species of marine fish, freshwater fish and shellfish.	1964	American Oil Chemists' Society, Journal	41	10	662	667"
        madeRow = self.reader.makeRow(row)
        self.assertEqual(len(madeRow),9)
        row = 'S10   	Food and Drug Administration (FDA), DHHS	FDA Total Diet Study	1995'
        madeRow = self.reader.makeRow(row)
        self.assertEqual(len(madeRow),4)

    def testFindCopyInstruction(self):
        self.reader.openFile('test/usda_test.sql')
        self.assertTrue(self.reader.findCopyInstruction())
        self.assertTrue(self.reader.findCopyInstruction())
        self.reader.closeFile()

    def testFindCreateTable(self):
        self.reader.openFile('test/usda_test.sql')
        self.assertEqual(self.reader.findCreateTable(),'data_src')
        self.reader.closeFile()

    def testMakeTable(self):
        self.reader.openFile('test/usda_test.sql')
        self.assertEqual(self.reader.findCreateTable(), 'data_src')
        expectedColumns = ['datasrc_id','authors','title','"year"','journal','vol_city','issue_state','start_page','end_page']
        self.assertEqual(self.reader.makeTable(),expectedColumns)
        self.reader.closeFile()
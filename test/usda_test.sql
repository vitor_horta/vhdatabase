CREATE TABLE data_src (
    datasrc_id character(6) NOT NULL,
    authors text,
    title text NOT NULL,
    "year" integer,
    journal text,
    vol_city text,
    issue_state text,
    start_page text,
    end_page text
);

CREATE TABLE datsrcln (
    ndb_no character(5) NOT NULL,
    nutr_no character(3) NOT NULL,
    datasrc_id character(6) NOT NULL
);
CREATE TABLE nut_data (
    ndb_no character(5) NOT NULL,
    nutr_no character(3) NOT NULL,
    nutr_val double precision NOT NULL,
    num_data_pts double precision NOT NULL,
    std_error double precision,
    src_cd integer NOT NULL,
    deriv_cd text,
    ref_ndb_no character(5),
    add_nutr_mark character(1),
    num_studies integer,
    min double precision,
    max double precision,
    df integer,
    low_eb double precision,
    up_eb double precision,
    stat_cmt text,
    cc character(1)
);


--
-- Name: nutr_def; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE nutr_def (
    nutr_no character(3) NOT NULL,
    units text NOT NULL,
    tagname text,
    nutrdesc text,
    num_dec smallint,
    sr_order integer
);


COPY data_src (datasrc_id, authors, title, "year", journal, vol_city, issue_state, start_page, end_page) FROM stdin;
D1066 	G.V. Mann	The Health and Nutritional status of Alaskan Eskimos.	1962	American Journal of Clinical Nutrition	11		31	76
D1073 	J.P. McBride, R.A. Maclead	Sodium and potassium in fish from the Canadian Pacific coast.	1956	Journal of the American Dietetic Association	32		636	638
D1107 	M.E. Stansby	Chemical Characteristics of fish caught in the northwest Pacific Oceans.	1976	Marine Fish Rev.	38	9	1	11
D1191 	I.J. Tinsley, R.R. Lowry	Bromine content of lipids of marine organisms	1980	American Oil Chemists' Society, Journal		1	31	33
D1243 	M. Iwasaki, R. Harada	Composition of the Ros Marine Species.	1985	(Pre-publication copy)
\.

COPY datsrcln (ndb_no, nutr_no, datasrc_id) FROM stdin;
01001	203	D1066
01002	204	D1066
01004	430	D1243
01003	207	D1066
01001	255	D1066
15078	301	D1066
15078	203	D1107
15078	204	D1107
15078	207	D1107
15078	255	D1107
15078	255	00000
\.

COPY nut_data (ndb_no, nutr_no, nutr_val, num_data_pts, std_error, src_cd, deriv_cd, ref_ndb_no, add_nutr_mark, num_studies, min, max, df, low_eb, up_eb, stat_cmt, cc) FROM stdin;
01001	205	0.059999999999999998	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	208	717	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	262	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01001	263	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01001	269	0.059999999999999998	0	\N	4	NR	     	 	\N	\N	\N	\N	\N	\N		\N
01001	301	24	17	0.78900000000000003	1	A	     	 	7	19	30	4	22.021000000000001	26.495999999999999	2, 3	\N
01001	303	0.02	18	0.010999999999999999	1	A	     	 	7	0	0.14999999999999999	7	-0.0080000000000000002	0.045999999999999999	2, 3	\N
01001	304	2	18	0.047	1	A	     	 	7	1	2	4	1.5860000000000001	1.863	2, 3	\N
01001	305	24	17	0.46300000000000002	1	A	     	 	7	19	27	7	22.488	24.652999999999999	2, 3	\N
01001	306	24	18	0.622	1	A	     	 	7	17	28	2	20.879000000000001	26.565999999999999	2, 3	\N
01001	307	576	18	12.006	1	A	     	 	7	491	685	5	545.01300000000003	605.98699999999997	2, 3	\N
01001	309	0.089999999999999997	18	0.010999999999999999	1	A	     	 	7	0.050000000000000003	0.26000000000000001	5	0.057000000000000002	0.11600000000000001	2, 3	\N
01001	312	0	18	0	1	A	     	 	7	0	0	\N	\N	\N	2, 3	\N
01001	315	0	18	0	1	A	     	 	7	0	0	\N	\N	\N	2, 3	\N
01001	317	1	37	0.81999999999999995	1	A	     	 	\N	\N	\N	\N	\N	\N		\N
01001	318	2499	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	319	671	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	320	684	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	321	158	2	\N	1	A	     	 	\N	\N	\N	\N	\N	\N		\N
01001	322	0	2	\N	1	A	     	 	\N	\N	\N	\N	\N	\N		\N
01001	324	56	0	\N	4	T	     	 	\N	\N	\N	\N	\N	\N		\N
01001	334	0	2	\N	1	A	     	 	\N	\N	\N	\N	\N	\N		\N
01001	337	0	2	\N	1	A	     	 	\N	\N	\N	\N	\N	\N		\N
01001	338	0	2	\N	1	A	     	 	\N	\N	\N	\N	\N	\N		\N
01001	431	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01001	435	3	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	573	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01001	578	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01001	601	215	3	1.2609999999999999	1	A	     	 	1	213	217	2	209.864	220.71700000000001	2, 3	\N
01001	606	51.368000000000002	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	607	3.226	3	0.12	1	A	     	 	1	3.1040000000000001	3.4660000000000002	2	2.71	3.7410000000000001	2, 3	\N
01001	608	2.0070000000000001	3	0.037999999999999999	1	A	     	 	1	1.9530000000000001	2.0800000000000001	2	1.845	2.1699999999999999	2, 3	\N
01001	609	1.1899999999999999	3	0.016	1	A	     	 	1	1.161	1.2150000000000001	2	1.1220000000000001	1.2569999999999999	2, 3	\N
01001	610	2.5289999999999999	3	0.033000000000000002	1	A	     	 	1	2.4910000000000001	2.5950000000000002	2	2.3849999999999998	2.6720000000000002	2, 3	\N
01001	611	2.5870000000000002	3	0.045999999999999999	1	A	     	 	1	2.508	2.6669999999999998	2	2.3889999999999998	2.7839999999999998	2, 3	\N
01001	612	7.4359999999999999	3	0.02	1	A	     	 	1	7.4089999999999998	7.4749999999999996	2	7.3520000000000003	7.5209999999999999	2, 3	\N
01001	613	21.696999999999999	3	0.27000000000000002	1	A	     	 	1	21.271999999999998	22.196999999999999	2	20.536999999999999	22.856999999999999	2, 3	\N
01001	614	9.9990000000000006	3	0.096000000000000002	1	A	     	 	1	9.8140000000000001	10.137	2	9.5860000000000003	10.412000000000001	2, 3	\N
01001	615	0.13800000000000001	3	0.002	1	A	     	 	1	0.13500000000000001	0.14199999999999999	2	0.129	0.14599999999999999	2, 3	\N
01001	617	19.960999999999999	0	\N	1	AS	     	 	\N	\N	\N	\N	\N	\N		\N
01001	618	2.7280000000000002	0	\N	1	AS	     	 	\N	\N	\N	\N	\N	\N		\N
01001	619	0.315	0	\N	1	AS	     	 	\N	\N	\N	\N	\N	\N		\N
01001	620	0	3	0	1	A	     	 	1	0	0	\N	\N	\N	1, 2, 3	\N
01001	621	0	3	0	1	A	     	 	1	0	0	\N	\N	\N	1, 2, 3	\N
01001	626	0.96099999999999997	0	\N	1	AS	     	 	\N	\N	\N	\N	\N	\N		\N
01001	627	0	3	0	1	A	     	 	1	0	0	\N	\N	\N	1, 2, 3	\N
01001	628	0.10000000000000001	3	0.0089999999999999993	1	A	     	 	1	0.084000000000000005	0.114	2	0.063	0.13700000000000001	2, 3	\N
01001	629	0	3	0	1	A	     	 	1	0	0	\N	\N	\N	1, 2, 3	\N
01001	630	0	3	0	1	A	     	 	1	0	0	\N	\N	\N	1, 2, 3	\N
01001	631	0	3	0	1	A	     	 	1	0	0	\N	\N	\N	1, 2, 3	\N
01001	638	0	3	0	1	A	     	 	1	0	0	\N	\N	\N	1, 2, 3	\N
01001	639	0	3	0	1	A	     	 	1	0	0	\N	\N	\N	1, 2, 3	\N
01001	641	4	3	0.11799999999999999	1	A	     	 	1	4	4	2	3.5659999999999998	4.5800000000000001	2, 3	\N
01001	645	21.021000000000001	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	646	3.0430000000000001	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01001	653	0.56000000000000005	3	0.014999999999999999	1	A	     	 	1	0.53000000000000003	0.57999999999999996	2	0.49399999999999999	0.625	2, 3	\N
01001	663	2.9820000000000002	3	0.112	1	A	     	 	1	2.7629999999999999	3.1280000000000001	2	2.5009999999999999	3.4630000000000001	2, 3	\N
01001	666	0.29599999999999999	3	0.048000000000000001	1	A	     	 	1	0.20100000000000001	0.35899999999999999	2	0.088999999999999996	0.503	2, 3	\N
01001	670	0.26700000000000002	3	0.017000000000000001	1	A	     	 	1	0.24099999999999999	0.29899999999999999	2	0.19400000000000001	0.34000000000000002	2, 3	\N
01001	673	0.96099999999999997	3	0.0080000000000000002	1	A	     	 	1	0.94599999999999995	0.97199999999999998	2	0.92800000000000005	0.99299999999999999	2, 3	\N
01001	674	16.978000000000002	3	0.39700000000000002	1	A	     	 	1	16.206	17.523	2	15.271000000000001	18.686	2, 3	\N
01001	675	2.1659999999999999	3	0.024	1	A	     	 	1	2.1259999999999999	2.2080000000000002	2	2.0640000000000001	2.2679999999999998	2, 3	\N
01001	851	0.315	3	0.012999999999999999	1	A	     	 	1	0.28899999999999998	0.33000000000000002	2	0.25900000000000001	0.371	2, 3	\N
01002	205	0.059999999999999998	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01002	208	717	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01002	262	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01002	263	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01002	269	0.059999999999999998	0	\N	4	NR	     	 	\N	\N	\N	\N	\N	\N		\N
01002	317	1	37	0.81999999999999995	1	A	     	 	\N	\N	\N	\N	\N	\N		\N
01002	318	2499	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01002	319	671	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01002	320	684	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01002	321	158	0	\N	4	BFFN	01001	 	\N	\N	\N	\N	\N	\N		\N
01002	322	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01002	323	2.3199999999999998	0	\N	4	BFFN	01001	 	\N	\N	\N	\N	\N	\N		\N
01002	334	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01002	337	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01002	338	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01002	430	7	0	\N	4	BFFN	01001	 	\N	\N	\N	\N	\N	\N		\N
01002	431	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01002	435	3	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01002	573	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01002	578	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	208	876	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01003	262	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	263	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	269	0	0	\N	4	NR	     	 	\N	\N	\N	\N	\N	\N		\N
01003	317	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	318	3069	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01003	319	824	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01003	320	840	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01003	321	193	0	\N	4	BFFN	01001	 	\N	\N	\N	\N	\N	\N		\N
01003	322	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	334	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	337	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	338	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	430	8.5999999999999996	0	\N	4	BFFN	01001	 	\N	\N	\N	\N	\N	\N		\N
01003	431	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	435	0	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01003	573	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01003	578	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01004	205	2.3399999999999999	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01004	208	353	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01004	262	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01004	263	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01004	269	0.5	0	\N	4	BFNN	01009	 	\N	\N	\N	\N	\N	\N		\N
01004	317	14.5	0	\N	4	CAZN	     	 	\N	\N	\N	\N	\N	\N		\N
01004	318	763	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01004	319	192	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01004	320	198	0	\N	4	NC	     	 	\N	\N	\N	\N	\N	\N		\N
01004	321	74	0	\N	4	BFFN	01009	 	\N	\N	\N	\N	\N	\N		\N
01004	322	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
01004	323	0.25	0	\N	4	BFFN	01009	 	\N	\N	\N	\N	\N	\N		\N
01004	334	0	0	\N	7	Z	     	 	\N	\N	\N	\N	\N	\N		\N
\.

COPY nutr_def (nutr_no, units, tagname, nutrdesc, num_dec, sr_order) FROM stdin;
203	g	PROCNT	Protein	2	600
204	g	FAT	Total lipid (fat)	2	800
205	g	CHOCDF	Carbohydrate, by difference	2	1100
207	g	ASH	Ash	2	1000
208	kcal	ENERC_KCAL	Energy	0	300
209	g	STARCH	Starch	2	2200
210	g	SUCS	Sucrose	2	1600
211	g	GLUS	Glucose (dextrose)	2	1700
212	g	FRUS	Fructose	2	1800
213	g	LACS	Lactose	2	1900
214	g	MALS	Maltose	2	2000
221	g	ALC	Alcohol, ethyl	1	18200
255	g	WATER	Water	2	100
257	g		Adjusted Protein	2	700
262	mg	CAFFN	Caffeine	0	18300
263	mg	THEBRN	Theobromine	0	18400
268	kj	ENERC_KJ	Energy	0	400
269	g	SUGAR	Sugars, total	2	1500
287	g	GALS	Galactose	2	2100
291	g	FIBTG	Fiber, total dietary	1	1200
301	mg	CA	Calcium, Ca	0	5300
303	mg	FE	Iron, Fe	2	5400
304	mg	MG	Magnesium, Mg	0	5500
305	mg	P	Phosphorus, P	0	5600
306	mg	K	Potassium, K	0	5700
307	mg	NA	Sodium, Na	0	5800
309	mg	ZN	Zinc, Zn	2	5900
312	mg	CU	Copper, Cu	3	6000
315	mg	MN	Manganese, Mn	3	6100
317	mcg	SE	Selenium, Se	1	6200
318	IU	VITA_IU	Vitamin A, IU	0	7400
319	mcg	RETOL	Retinol	0	7700
320	mcg_RAE	VITA_RAE	Vitamin A, RAE	0	7499
321	mcg	CARTB	Carotene, beta	0	18500
322	mcg	CARTA	Carotene, alpha	0	18600
323	mg	TOCPHA	Vitamin E (alpha-tocopherol)	2	7900
324	IU	VITD-	Vitamin D	0	8700
334	mcg	CRYPX	Cryptoxanthin, beta	0	18700
337	mcg	LYCPN	Lycopene	0	18800
338	mcg	LUT+ZEA	Lutein + zeaxanthin	0	18900
341	mg	TOCPHB	Tocopherol, beta	2	8000
342	mg	TOCPHG	Tocopherol, gamma	2	8100
343	mg	TOCPHD	Tocopherol, delta	2	8200
401	mg	VITC	Vitamin C, total ascorbic acid	1	6300
404	mg	THIA	Thiamin	3	6400
405	mg	RIBF	Riboflavin	3	6500
406	mg	NIA	Niacin	3	6600
410	mg	PANTAC	Pantothenic acid	3	6700
415	mg	VITB6A	Vitamin B-6	3	6800
417	mcg	FOL	Folate, total	0	6900
418	mcg	VITB12	Vitamin B-12	2	7300
430	mcg	VITK	Vitamin K (phylloquinone)	1	8800
431	mcg	FOLAC	Folic acid	0	7000
432	mcg	FOLFD	Folate, food	0	7100
435	mcg_DFE	FOLDFE	Folate, DFE	0	7200
501	g	TRP_G	Tryptophan	3	16300
502	g	THR_G	Threonine	3	16400
503	g	ILE_G	Isoleucine	3	16500
504	g	LEU_G	Leucine	3	16600
505	g	LYS_G	Lysine	3	16700
506	g	MET_G	Methionine	3	16800
507	g	CYS_G	Cystine	3	16900
508	g	PHE_G	Phenylalanine	3	17000
509	g	TYR_G	Tyrosine	3	17100
510	g	VAL_G	Valine	3	17200
511	g	ARG_G	Arginine	3	17300
512	g	HISTN_G	Histidine	3	17400
513	g	ALA_G	Alanine	3	17500
514	g	ASP_G	Aspartic acid	3	17600
515	g	GLU_G	Glutamic acid	3	17700
516	g	GLY_G	Glycine	3	17800
517	g	PRO_G	Proline	3	17900
518	g	SER_G	Serine	3	18000
521	g	HYP	Hydroxyproline	3	18100
573	mg		Vitamin E, added	2	7920
578	mcg		Vitamin B-12, added	2	7340
601	mg	CHOLE	Cholesterol	0	15700
605	g	FATRN	Fatty acids, total trans	3	15400
606	g	FASAT	Fatty acids, total saturated	3	9700
607	g	F4D0	4:0	3	9800
608	g	F6D0	6:0	3	9900
609	g	F8D0	8:0	3	10000
610	g	F10D0	10:0	3	10100
611	g	F12D0	12:0	3	10300
612	g	F14D0	14:0	3	10500
613	g	F16D0	16:0	3	10700
614	g	F18D0	18:0	3	10900
615	g	F20D0	20:0	3	11100
617	g	F18D1	18:1 undifferentiated	3	12100
618	g	F18D2	18:2 undifferentiated	3	13100
619	g	F18D3	18:3 undifferentiated	3	13900
620	g	F20D4	20:4 undifferentiated	3	14700
621	g	F22D6	22:6 n-3	3	15300
624	g	F22D0	22:0	3	11200
625	g	F14D1	14:1	3	11500
626	g	F16D1	16:1 undifferentiated	3	11700
627	g	F18D4	18:4	3	14200
628	g	F20D1	20:1	3	12400
629	g	F20D5	20:5 n-3	3	15000
630	g	F22D1	22:1 undifferentiated	3	12500
631	g	F22D5	22:5 n-3	3	15200
636	mg	PHYSTR	Phytosterols	0	15800
638	mg	STID7	Stigmasterol	0	15900
639	mg	CAMD5	Campesterol	0	16000
641	mg	SITSTR	Beta-sitosterol	0	16200
645	g	FAMS	Fatty acids, total monounsaturated	3	11400
646	g	FAPU	Fatty acids, total polyunsaturated	3	12900
652	g	F15D0	15:0	3	10600
653	g	F17D0	17:0	3	10800
654	g	F24D0	24:0	3	11300
662	g	F16D1T	16:1 t	3	11900
663	g	F18D1T	18:1 t	3	12300
664	g		22:1 t	3	12700
665	g		18:2 t not further defined	3	13800
666	g		18:2 i	3	13700
669	g		18:2 t,t	3	13600
670	g	F18D2CLA	18:2 CLAs	3	13300
671	g	F24D1C	24:1 c	3	12800
672	g	F20D2CN6	20:2 n-6 c,c	3	14300
673	g	F16D1C	16:1 c	3	11800
674	g	F18D1C	18:1 c	3	12200
675	g	F18D2CN6	18:2 n-6 c,c	3	13200
676	g		22:1 c	3	12600
685	g	F18D3CN6	18:3 n-6 c,c,c	3	14100
687	g	F17D1	17:1	3	12000
689	g	F20D3	20:3 undifferentiated	3	14400
693	g	FATRNM	Fatty acids, total trans-monoenoic	3	15500
695	g	FATRNP	Fatty acids, total trans-polyenoic	3	15600
696	g	F13D0	13:0	3	10400
697	g	F15D1	15:1	3	11600
851	g	F18D3CN3	18:3 n-3 c,c,c	3	14000
853	g	F20D3N6	20:3 n-6	3	14600
855	g	F20D4N6	20:4 n-6	3	14900
856	g		18:3i	3	14200
857	g	F21D5	21:5	3	15100
858	g	F22D4	22:4	3	15160
\.

COPY testtable (testtable_id, authors, title, "year", journal, vol_city, issue_state, start_page, end_page) FROM stdin;
\.

ALTER TABLE ONLY data_src
    ADD CONSTRAINT data_src_pkey PRIMARY KEY (datasrc_id);


--
-- Name: datsrcln_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY datsrcln
    ADD CONSTRAINT datsrcln_pkey PRIMARY KEY (ndb_no, nutr_no, datasrc_id);

ALTER TABLE ONLY nut_data
    ADD CONSTRAINT nut_data_pkey PRIMARY KEY (ndb_no, nutr_no);

ALTER TABLE ONLY nutr_def
    ADD CONSTRAINT nutr_def_pkey PRIMARY KEY (nutr_no);

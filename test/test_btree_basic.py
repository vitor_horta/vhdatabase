import unittest
from BinarySearchTree.BinarySearchTree import BinarySearchTree
from vhdatabase.VHRow import VHRow
from collections import namedtuple

class TestBTreeBasic(unittest.TestCase):
    def setUp(self):
        self.tree = BinarySearchTree()
        self.recordType = namedtuple('row', ['id'])
        row1 = VHRow([3],['id'],self.recordType)
        row2 = VHRow([2],['id'],self.recordType)
        row3 = VHRow([7],['id'],self.recordType)
        row4 = VHRow([1],['id'],self.recordType)
        row5 = VHRow([9],['id'],self.recordType)
        row6 = VHRow([6],['id'],self.recordType)
        self.tree.addItem(row1)
        self.tree.addItem(row2)
        self.tree.addItem(row3)
        self.tree.addItem(row4)
        self.tree.addItem(row5)
        self.tree.addItem(row6)
        self.resultedArray = []

    def compareValues(self,x):
        self.resultedArray.append(x._value[0])

    def testAddInts(self):
        tree = BinarySearchTree()
        row1 = VHRow([3],['id'],self.recordType)
        row2 = VHRow([2],['id'],self.recordType)
        row3 = VHRow([7],['id'],self.recordType)
        row4 = VHRow([1],['id'],self.recordType)
        row5 = VHRow([9],['id'],self.recordType)
        row6 = VHRow([6],['id'],self.recordType)
        self.assertTrue(tree.addItem(row1))
        self.assertTrue(tree.addItem(row2))
        self.assertTrue(tree.addItem(row3))
        self.assertTrue(tree.addItem(row4))
        self.assertTrue(tree.addItem(row5))
        self.assertTrue(tree.addItem(row6))
        self.assertEqual(tree.getKey(),'#'+str(3))
        self.assertEqual(tree._root._left.getKey(),'#'+str(2))
        self.assertEqual(tree._root._right.getKey(),'#'+str(7))
        self.assertEqual(tree._root._left._left.getKey(),'#'+str(1))
        self.assertIsNone(tree._root._left._right)
        self.assertEqual(tree._root._right._left.getKey(), '#'+str(6))
        self.assertEqual(tree._root._right._right.getKey(), '#'+str(9))

    def testAddIntsStr(self):
        tree = BinarySearchTree()
        row1 = VHRow(['3'],['id'],self.recordType)
        row2 = VHRow(['2'],['id'],self.recordType)
        row3 = VHRow(['7'],['id'],self.recordType)
        row4 = VHRow(['1'],['id'],self.recordType)
        row5 = VHRow(['9'],['id'],self.recordType)
        row6 = VHRow(['6'],['id'],self.recordType)
        self.assertTrue(tree.addItem(row1))
        self.assertTrue(tree.addItem(row2))
        self.assertTrue(tree.addItem(row3))
        self.assertTrue(tree.addItem(row4))
        self.assertTrue(tree.addItem(row5))
        self.assertTrue(tree.addItem(row6))
        self.assertEqual(tree.getKey(),'#'+str(3))
        self.assertEqual(tree._root._left.getKey(),'#'+str(2))
        self.assertEqual(tree._root._right.getKey(),'#'+str(7))
        self.assertEqual(tree._root._left._left.getKey(),'#'+str(1))
        self.assertIsNone(tree._root._left._right)
        self.assertEqual(tree._root._right._left.getKey(),'#'+ str(6))
        self.assertEqual(tree._root._right._right.getKey(), '#'+str(9))

    def testAddStr(self):
        tree = BinarySearchTree()
        row1 = VHRow(['c'],['id'],self.recordType)
        row2 = VHRow(['b'],['id'],self.recordType)
        row3 = VHRow(['g'],['id'],self.recordType)
        row4 = VHRow(['a'],['id'],self.recordType)
        row5 = VHRow(['i'],['id'],self.recordType)
        row6 = VHRow(['f'],['id'],self.recordType)
        self.assertTrue(tree.addItem(row1))
        self.assertTrue(tree.addItem(row2))
        self.assertTrue(tree.addItem(row3))
        self.assertTrue(tree.addItem(row4))
        self.assertTrue(tree.addItem(row5))
        self.assertTrue(tree.addItem(row6))
        self.assertEqual(tree.getKey(),'#c')
        self.assertEqual(tree._root._left.getKey(),'#b')
        self.assertEqual(tree._root._right.getKey(),'#g')
        self.assertEqual(tree._root._left._left.getKey(),str('#a'))
        self.assertIsNone(tree._root._left._right)
        self.assertEqual(tree._root._right._left.getKey(), str('#f'))
        self.assertEqual(tree._root._right._right.getKey(), str('#i'))

    def testPreOrderTraversal(self):
        expectedArray = ['3','2','1','7','6','9']
        self.tree.preOrderTraversal(self.compareValues)
        self.assertEqual(self.resultedArray,expectedArray)

    def testInOrderTraversal(self):
        expectedArray = ['1','2','3','6','7','9']
        self.tree.inOrderTraversal(self.compareValues)
        self.assertEqual(self.resultedArray,expectedArray)

    def testPostOrderTraversal(self):
        expectedArray = ['1','2','3','6','7','9']
        self.tree.inOrderTraversal(self.compareValues)
        self.assertEqual(self.resultedArray,expectedArray)

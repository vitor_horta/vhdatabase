import unittest
from vhdatabase.VHDatabase import VHDatabase
from vhdatabase.VHTable import VHTable
from vhdatabase.constants import *

class TestVHTable(unittest.TestCase):
    def setUp(self):
        self.linkedTable = VHTable('t1',['id','name'],['id'])
        self.bsTreeTable = VHTable('t1',['id','name'],['id'],INDEX_TYPE_BSTREE)

    def testIsBSTree(self):
        self.assertTrue(isinstance(self.bsTreeTable._rows,INDEX_TYPE_BSTREE))

    def testIsLinked(self):
        self.assertTrue(isinstance(self.linkedTable._rows,INDEX_TYPE_LINKEDLIST))

    def testLinkedAddRow(self):
        self.assertTrue(self.linkedTable.addRow(['1','vitor']))

    def testBSTAddRow(self):
        self.assertTrue(self.bsTreeTable.addRow(['1','vitor']))

    def testGetItem(self):
        row = ['1','vitor']
        row2 = ['2','jose']
        self.linkedTable.addRow(row)
        self.linkedTable.addRow(row2)
        self.assertEqual(list(self.linkedTable.getItem(row[0])),row)
        self.assertEqual(list(self.linkedTable.getItem(row2[0])),row2)

    def testGetItemTwoKeys(self):
        twoKeysTable = VHTable('t1',['id','name','city'],['id','city'])
        row = ['1', 'vitor','JF']
        row2 = ['2', 'jose','BH']
        twoKeysTable.addRow(row)
        twoKeysTable.addRow(row2)
        rowKeys = [row[0],row[2]]
        rowKeys2 = [row2[0],row2[2]]
        self.assertEqual(list(twoKeysTable.getItem(rowKeys)), row)
        self.assertEqual(list(twoKeysTable.getItem(rowKeys2)), row2)

    def testBSGetItemTwoKeys(self):
        twoKeysTable = VHTable('t1', ['id', 'name', 'city'], ['id', 'city'],INDEX_TYPE_BSTREE)
        row = ['1', 'vitor', 'JF']
        row2 = ['2', 'jose', 'BH']
        twoKeysTable.addRow(row)
        twoKeysTable.addRow(row2)
        rowKeys = [row[0],row[2]]
        rowKeys2 = [row2[0],row2[2]]
        self.assertEqual(list(twoKeysTable.getItem(rowKeys)), row)
        self.assertEqual(list(twoKeysTable.getItem(rowKeys2)), row2)

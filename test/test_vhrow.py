import unittest
from vhdatabase.VHRow import VHRow
from collections import namedtuple

class TestVHRow(unittest.TestCase):
    def setUp(self):
        self.recordType = namedtuple('row', ['id','nome','ano'])

    def testRowOneKey(self):
        row = ['1','Vitor','1991']
        vhrow = VHRow(row,['id'],self.recordType)
        self.assertEqual(list(vhrow._value),row)
        self.assertEqual(vhrow.getKey(),'#1')

    def testRowTwosKeys(self):
        row = ['1','Vitor','1991']
        vhrow = VHRow(row,['id','nome'],self.recordType)
        self.assertEqual(list(vhrow._value),row)
        self.assertEqual(vhrow.getKey(),'#1'+'#Vitor')

    def testRowOneAndThirdKeys(self):
        row = ['1', 'Vitor', '1991']
        vhrow = VHRow(row,['id','ano'],self.recordType)
        self.assertEqual(list(vhrow._value), row)
        self.assertEqual(vhrow.getKey(), '#1' + '#1991')

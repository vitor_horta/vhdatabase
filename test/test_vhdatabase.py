import unittest
from vhdatabase.VHDatabase import VHDatabase
from vhdatabase.constants import *

class TestVHDB(unittest.TestCase):
    def setUp(self):
        self.db = VHDatabase()

    def testAddTable(self):
        self.assertTrue(self.db.addTable('t1',['f1'],['f1']))
        self.assertRaises(NameError,self.db.addTable,'t1',['f1'],['f1'])
        self.assertRaises(TypeError,self.db.addTable,'t1',[])
        self.assertRaises(TypeError,self.db.addTable,'t1')

    def testAddRow(self):
        self.db.addTable('table',['id','name'],['id'])
        row = ['199','vitor']
        self.assertTrue(self.db.addRow('table',row))
        falseRow = []
        self.assertFalse(self.db.addRow('table', falseRow))
        falseRow = False
        self.assertFalse(self.db.addRow('table', falseRow))
        falseRow = ''
        self.assertFalse(self.db.addRow('table', falseRow))

    def testGetItem(self):
        self.db.addTable('table', ['id', 'name'], ['id'])
        row = ['199', 'vitor']
        self.db.addRow('table', row)
        self.assertEqual(list(self.db.getItem('table',['199'])),row)


    def testReadTablesFromFile(self):
        self.db.readTablesFromFile('test/usda_test.sql')
        self.assertTrue(self.db.containsTable('data_src'))
        self.assertTrue(self.db.containsTable('datsrcln'))

    def testReadRowsFromFile(self):
        self.db.readTablesFromFile('test/usda_test.sql')
        self.db.readRowsFromFile('test/usda_test.sql')
        self.assertIsInstance(list(self.db.getItem('data_src',['D1066'])),list)
        self.assertFalse(self.db.getItem('data_src','D10asdfasf66'))

    def testGetAllItens(self):
        tableName = 'allitens'
        self.db.addTable(tableName, ['id', 'name'], ['id'])
        row = ['199', 'vitor']
        self.db.addRow(tableName, row)
        row1 = ['200', 'teste']
        self.db.addRow(tableName, row1)
        row2 = ['201', 'joao']
        self.db.addRow(tableName, row2)

        all = self.db.getAllItens(tableName)
        self.assertEqual(len(all),3)
        self.assertEqual(all[2].id,row[0])
        self.assertEqual(all[1].id,row1[0])
        self.assertEqual(all[0].id,row2[0])

    def testGetItemWhere(self):
        tableName = 'itemwhere'
        self.db.addTable(tableName, ['id', 'nome','cidade'], ['id'], INDEX_TYPE_SKIPLIST)
        row = ['199', 'vitor','JF']
        self.db.addRow(tableName, row)
        row1 = ['200', 'teste','JF']
        self.db.addRow(tableName, row1)

        item1 = self.db.getItemWhere(tableName,'id',row[0])[0]
        item2 = self.db.getItemWhere(tableName,'id',row1[0])[0]
        item3 = self.db.getItemWhere(tableName,'nome',row1[1])[0]
        self.assertEqual(item1.id,row[0])
        self.assertEqual(item2.id,row1[0])
        self.assertEqual(item3.id,row1[0])

        itens12 = self.db.getItemWhere(tableName,'cidade',row[2])
        self.assertEqual(len(itens12),2)

    def testQueriesList(self):
        self.db = None
        self.db = VHDatabase()
        self.db.readTablesFromFile('test/usda_test.sql', INDEX_TYPE_LIST)
        self.db.readRowsFromFile('test/usda_test.sql')
        self.db.sortTables()
        self.executeSqls()

    def testQueriesSkiplist(self):
        self.db = None
        self.db = VHDatabase()
        self.db.readTablesFromFile('test/usda_test.sql', INDEX_TYPE_SKIPLIST)
        self.db.readRowsFromFile('test/usda_test.sql')
        self.executeSqls()

    def testQueriesLinkedList(self):
        self.db = None
        self.db = VHDatabase()
        self.db.readTablesFromFile('test/usda_test.sql', INDEX_TYPE_LINKEDLIST)
        self.db.readRowsFromFile('test/usda_test.sql')
        self.executeSqls()

    def testQueriesAvlTree(self):
        self.db = None
        self.db = VHDatabase()
        self.db.readTablesFromFile('test/usda_test.sql', INDEX_TYPE_BSTREE)
        self.db.readRowsFromFile('test/usda_test.sql')
        self.executeSqls()

    def testDeleteRowLinkedList(self):
        self.db = None
        self.db = VHDatabase()
        self.db.readTablesFromFile('test/usda_test.sql', INDEX_TYPE_LINKEDLIST)
        self.db.readRowsFromFile('test/usda_test.sql')
        sqlSelect = "DELETE FROM data_src D1066"
        self.db.parseSql(sqlSelect)
        sqlSelect = "SELECT COUNT FROM data_src"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 4)

        sqlSelect = "DELETE FROM data_src D1107"
        self.db.parseSql(sqlSelect)
        sqlSelect = "SELECT COUNT FROM data_src"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 3)

    def testDeleteRowList(self):
        self.db = None
        self.db = VHDatabase()
        self.db.readTablesFromFile('test/usda_test.sql', INDEX_TYPE_LIST)
        self.db.readRowsFromFile('test/usda_test.sql')
        self.db.sortTables()
        sqlSelect = "DELETE FROM data_src D1066"
        self.db.parseSql(sqlSelect)
        sqlSelect = "SELECT COUNT FROM data_src"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 4)

        sqlSelect = "DELETE FROM data_src D1107"
        self.db.parseSql(sqlSelect)
        sqlSelect = "SELECT COUNT FROM data_src"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 3)

    def testDeleteRowSkipList(self):
        self.db = None
        self.db = VHDatabase()
        self.db.readTablesFromFile('test/usda_test.sql', INDEX_TYPE_SKIPLIST)
        self.db.readRowsFromFile('test/usda_test.sql')
        sqlSelect = "DELETE FROM data_src D1066"
        self.db.parseSql(sqlSelect)
        sqlSelect = "SELECT COUNT FROM data_src"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 4)

        sqlSelect = "DELETE FROM data_src D1107"
        self.db.parseSql(sqlSelect)
        sqlSelect = "SELECT COUNT FROM data_src"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 3)

    def executeSqls(self):
        sqlSelect = "SELECT FROM data_src"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(len(result),5)
        sqlSelect = "SELECT FROM datsrcln"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(len(result),11)

        #TEST COUNT
        sqlSelect = "SELECT COUNT FROM data_src"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 5)
        sqlSelect = "SELECT COUNT FROM datsrcln"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 11)

        #TEST WHERES
        sqlSelect = "SELECT FROM data_src WHERE datasrc_id = D1066"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(len(result), 1)
        sqlSelect = "SELECT FROM datsrcln WHERE datasrc_id = D1066"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(len(result), 5)

        # TEST COUNT WHERE
        sqlSelect = "SELECT COUNT FROM data_src WHERE datasrc_id = D1066"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 1)
        sqlSelect = "SELECT COUNT FROM datsrcln WHERE datasrc_id = D1066"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 5)

        # TEST WHERES
        sqlSelect = "SELECT COUNT FROM data_src WHERE year = 1985"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 1)

        #TEST COUNT INNER JOIN
        sqlSelect = "SELECT COUNT FROM data_src INNER JOIN datsrcln ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 10)

        # TEST COUNT RIGHT JOIN
        sqlSelect = "SELECT COUNT FROM data_src RIGHT JOIN datsrcln ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 11)

        # TEST COUNT RIGHT JOIN
        sqlSelect = "SELECT COUNT FROM datsrcln RIGHT JOIN data_src ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 12)

        # TEST COUNT LEFT JOIN
        sqlSelect = "SELECT COUNT FROM data_src LEFT JOIN datsrcln ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 12)

        # TEST COUNT LEFT JOIN
        sqlSelect = "SELECT COUNT FROM datsrcln LEFT JOIN data_src ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 11)

        # TEST COUNT RIGHT JOIN COM WHERE
        sqlSelect = "SELECT COUNT FROM data_src RIGHT JOIN datsrcln ON datasrc_id = datasrc_id WHERE datasrc_id = D1066"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 5)

        # TEST COUNT LEFT JOIN COM WHERE
        sqlSelect = "SELECT COUNT FROM data_src LEFT JOIN datsrcln ON datasrc_id = datasrc_id WHERE datasrc_id = D1066"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 5)

        #TEST INNER JOINS
        sqlSelect = "SELECT FROM data_src INNER JOIN datsrcln ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(len(result), 10)
        sqlSelect = "SELECT FROM datsrcln INNER JOIN data_src ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(len(result), 10)

        # TEST WHERE AND INNER JOINS
        sqlSelect = "SELECT FROM data_src WHERE datasrc_id = D1107 INNER JOIN datsrcln ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(len(result), 4)
        sqlSelect = "SELECT FROM datsrcln WHERE datasrc_id = D1107 INNER JOIN data_src ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(len(result), 4)

        # TEST WHERE AND INNER JOIN AND COUNT
        sqlSelect = "SELECT COUNT FROM data_src WHERE datasrc_id = D1107 INNER JOIN datsrcln ON datasrc_id = datasrc_id"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 4)

        # TEST INNER JOIN AND COUNT BY FIRST PKEY
        sqlSelect = "SELECT COUNT FROM nut_data INNER JOIN datsrcln ON ndb_no = ndb_no"
        result = self.db.parseSql(sqlSelect)
        self.assertEqual(result[0].count, 177)
import unittest
from BinarySearchTree.BinarySearchTree import BinarySearchTree
from vhdatabase.VHRow import VHRow
from collections import namedtuple
from BinarySearchTree.AvlNode import AvlNode
class TestAvlTree(unittest.TestCase):
    def setUp(self):
        self.tree = BinarySearchTree(nodeType=AvlNode)
        self.recordType = namedtuple('row', ['id'])
        # row1 = VHRow([3],['id'],self.recordType)
        # row2 = VHRow([2],['id'],self.recordType)
        # row3 = VHRow([7],['id'],self.recordType)
        # row4 = VHRow([1],['id'],self.recordType)
        # row5 = VHRow([9],['id'],self.recordType)
        # row6 = VHRow([6],['id'],self.recordType)
        # self.tree.addItem(row1)
        # self.tree.addItem(row2)
        # self.tree.addItem(row3)
        # self.tree.addItem(row4)
        # self.tree.addItem(row5)
        # self.tree.addItem(row6)
        # self.resultedArray = []

    def testRotEsq(self):
        self.tree = None
        self.tree = BinarySearchTree(nodeType=AvlNode)

        row1 = VHRow([1], ['id'], self.recordType)
        row2 = VHRow([2], ['id'], self.recordType)
        row3 = VHRow([3], ['id'], self.recordType)
        self.tree.addItem(row1)
        self.tree.addItem(row2)
        self.tree.addItem(row3)
        self.assertEquals(self.tree._root,row2)
        self.assertEquals(self.tree._root._left,row1)
        self.assertEquals(self.tree._root._right,row3)

    def testRotDir(self):
        self.tree = None
        self.tree = BinarySearchTree(nodeType=AvlNode)

        row1 = VHRow([3], ['id'], self.recordType)
        row2 = VHRow([2], ['id'], self.recordType)
        row3 = VHRow([1], ['id'], self.recordType)
        self.tree.addItem(row1)
        self.tree.addItem(row2)
        self.tree.addItem(row3)
        self.assertEquals(self.tree._root,row2)
        self.assertEquals(self.tree._root._left,row3)
        self.assertEquals(self.tree._root._right,row1)

    def testRotEsqDir(self):
        self.tree = None
        self.tree = BinarySearchTree(nodeType=AvlNode)

        row1 = VHRow([5], ['id'], self.recordType)
        row2 = VHRow([3], ['id'], self.recordType)
        row3 = VHRow([4], ['id'], self.recordType)
        self.tree.addItem(row1)
        self.tree.addItem(row2)
        self.tree.addItem(row3)
        self.assertEquals(self.tree._root,row3)
        self.assertEquals(self.tree._root._left,row2)
        self.assertEquals(self.tree._root._right,row1)

    def testRotDirEsq(self):
        self.tree = None
        self.tree = BinarySearchTree(nodeType=AvlNode)

        row1 = VHRow([3], ['id'], self.recordType)
        row2 = VHRow([5], ['id'], self.recordType)
        row3 = VHRow([4], ['id'], self.recordType)
        self.tree.addItem(row1)
        self.tree.addItem(row2)
        self.tree.addItem(row3)
        self.assertEquals(self.tree._root,row3)
        self.assertEquals(self.tree._root._left,row1)
        self.assertEquals(self.tree._root._right,row2)

    def testSeisItens(self):
        self.tree = None
        self.tree = BinarySearchTree(nodeType=AvlNode)

        row1 = VHRow([5], ['id'], self.recordType)
        row2 = VHRow([7], ['id'], self.recordType)
        row3 = VHRow([3], ['id'], self.recordType)
        row4 = VHRow([4], ['id'], self.recordType)
        row5 = VHRow([2], ['id'], self.recordType)
        row6 = VHRow([1], ['id'], self.recordType)

        self.tree.addItem(row1)
        self.tree.addItem(row2)
        self.tree.addItem(row3)
        self.tree.addItem(row4)
        self.tree.addItem(row5)
        self.tree.addItem(row6)

        self.assertEquals(self.tree._root,row3)
        self.assertEquals(self.tree._root._left,row5)
        self.assertEquals(self.tree._root._left._left,row6)
        self.assertEquals(self.tree._root._right,row1)
        self.assertEquals(self.tree._root._right._right,row2)
        self.assertEquals(self.tree._root._right._left,row4)
import unittest
from BinarySearchTree.BinarySearchTree import BinarySearchTree
from vhdatabase.VHRow import VHRow
from collections import namedtuple
from BinarySearchTree.TreeNode import TreeNode

class TestBtree(unittest.TestCase):
    def setUp(self):
        self.btree = BinarySearchTree(nodeType=TreeNode)
        self.recordType = namedtuple('row', ['id'])

    def testRotateRightCase1(self):
        row1 = VHRow([20],['id'],self.recordType)
        row2 = VHRow([15],['id'],self.recordType)

        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.assertEqual(self.btree._root, row1)
        self.assertIsNone(self.btree._root._parent, None)
        self.assertEqual(self.btree._root._left._parent, self.btree._root)

        self.btree._root = self.btree.rotacionaDir(self.btree._root)
        self.assertEqual(self.btree._root,row2)
        self.assertEqual(self.btree._root._right,row1)

        # check parents
        self.assertEqual(self.btree._root._right._parent,self.btree._root)
        self.assertIsNone(self.btree._root._parent)

    def testRotateRightCase2(self):
        row1 = VHRow([20],['id'],self.recordType)
        row2 = VHRow([15],['id'],self.recordType)
        row3 = VHRow([10],['id'],self.recordType)

        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.btree.addItem(row3)
        self.assertEqual(self.btree._root, row1)
        self.btree._root = self.btree.rotacionaDir(self.btree._root)
        self.assertEqual(self.btree._root, row2)
        self.assertEqual(self.btree._root._right, row1)
        self.assertEqual(self.btree._root._left, row3)

        #check parents
        self.assertIsNone(self.btree._root._parent)
        self.assertEqual(self.btree._root._right._parent, self.btree._root)
        self.assertEqual(self.btree._root._left._parent, self.btree._root)

    def testRotateRightCase3(self):
        row1 = VHRow([20],['id'],self.recordType)
        row2 = VHRow([15],['id'],self.recordType)
        row3 = VHRow([10],['id'],self.recordType)
        row4 = VHRow([17],['id'],self.recordType)

        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.btree.addItem(row3)
        self.btree.addItem(row4)
        self.assertEqual(self.btree._root, row1)
        self.btree._root = self.btree.rotacionaDir(self.btree._root)
        self.assertEqual(self.btree._root, row2)
        self.assertEqual(self.btree._root._right, row1)
        self.assertEqual(self.btree._root._left, row3)
        self.assertEqual(self.btree._root._right._left, row4)

        # check parents
        self.assertIsNone(self.btree._root._parent)
        self.assertEqual(self.btree._root._right._parent, self.btree._root)
        self.assertEqual(self.btree._root._left._parent, self.btree._root)
        self.assertEqual(self.btree._root._right._left._parent, self.btree._root._right)

    def testRotateRightCase4(self):
        row1 = VHRow([20], ['id'], self.recordType)
        row2 = VHRow([15], ['id'], self.recordType)
        row3 = VHRow([10], ['id'], self.recordType)
        row4 = VHRow([17], ['id'], self.recordType)
        row5 = VHRow([25], ['id'], self.recordType)


        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.btree.addItem(row3)
        self.btree.addItem(row4)
        self.btree.addItem(row5)
        self.assertEqual(self.btree._root, row1)
        self.btree._root = self.btree.rotacionaDir(self.btree._root)
        self.assertEqual(self.btree._root, row2)
        self.assertEqual(self.btree._root._right, row1)
        self.assertEqual(self.btree._root._left, row3)
        self.assertEqual(self.btree._root._right._left, row4)
        self.assertEqual(self.btree._root._right._right, row5)

        # check parents
        self.assertIsNone(self.btree._root._parent)
        self.assertEqual(self.btree._root._right._parent, self.btree._root)
        self.assertEqual(self.btree._root._left._parent, self.btree._root)
        self.assertEqual(self.btree._root._right._left._parent, self.btree._root._right)
        self.assertEqual(self.btree._root._right._right._parent, self.btree._root._right)


    def testRotateLeftCase1(self):
        row1 = VHRow([15],['id'],self.recordType)
        row2 = VHRow([20],['id'],self.recordType)

        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.assertEqual(self.btree._root, row1)

        self.btree._root = self.btree.rotacionaEsq(self.btree._root)
        self.assertEqual(self.btree._root,row2)
        self.assertEqual(self.btree._root._left,row1)

        # check parents
        self.assertEqual(self.btree._root._left._parent,self.btree._root)
        self.assertIsNone(self.btree._root._parent)

    def testRotateLeftCase2(self):
        row1 = VHRow([10],['id'],self.recordType)
        row2 = VHRow([15],['id'],self.recordType)
        row3 = VHRow([20],['id'],self.recordType)

        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.btree.addItem(row3)
        self.assertEqual(self.btree._root, row1)
        self.btree._root = self.btree.rotacionaEsq(self.btree._root)
        self.assertEqual(self.btree._root, row2)
        self.assertEqual(self.btree._root._left, row1)
        self.assertEqual(self.btree._root._right, row3)

        #check parents
        self.assertIsNone(self.btree._root._parent)
        self.assertEqual(self.btree._root._right._parent, self.btree._root)
        self.assertEqual(self.btree._root._left._parent, self.btree._root)

    def testRotateLeftCase3(self):
        row1 = VHRow([10],['id'],self.recordType)
        row2 = VHRow([15],['id'],self.recordType)
        row3 = VHRow([20],['id'],self.recordType)
        row4 = VHRow([17],['id'],self.recordType)

        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.btree.addItem(row3)
        self.btree.addItem(row4)
        self.assertEqual(self.btree._root, row1)
        self.btree._root = self.btree.rotacionaEsq(self.btree._root)
        self.assertEqual(self.btree._root, row2)
        self.assertEqual(self.btree._root._left, row1)
        self.assertEqual(self.btree._root._right, row3)
        self.assertEqual(self.btree._root._right._left, row4)

        # check parents
        self.assertIsNone(self.btree._root._parent)
        self.assertEqual(self.btree._root._right._parent, self.btree._root)
        self.assertEqual(self.btree._root._left._parent, self.btree._root)
        self.assertEqual(self.btree._root._right._left._parent, self.btree._root._right)

    def testRotateLeftCase4(self):
        row1 = VHRow([10],['id'],self.recordType)
        row2 = VHRow([15],['id'],self.recordType)
        row3 = VHRow([20],['id'],self.recordType)
        row4 = VHRow([17],['id'],self.recordType)
        row5 = VHRow([25],['id'],self.recordType)


        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.btree.addItem(row3)
        self.btree.addItem(row4)
        self.btree.addItem(row5)
        self.assertEqual(self.btree._root, row1)
        self.btree._root = self.btree.rotacionaEsq(self.btree._root)
        self.assertEqual(self.btree._root, row2)
        self.assertEqual(self.btree._root._left, row1)
        self.assertEqual(self.btree._root._right, row3)
        self.assertEqual(self.btree._root._right._left, row4)
        self.assertEqual(self.btree._root._right._right, row5)

        # check parents
        self.assertIsNone(self.btree._root._parent)
        self.assertEqual(self.btree._root._right._parent, self.btree._root)
        self.assertEqual(self.btree._root._left._parent, self.btree._root)
        self.assertEqual(self.btree._root._right._left._parent, self.btree._root._right)
        self.assertEqual(self.btree._root._right._right._parent, self.btree._root._right)

    def testEspinhaDorsal(self):
        row1 = VHRow([10],['id'],self.recordType)
        row2 = VHRow([15],['id'],self.recordType)
        row3 = VHRow([17],['id'],self.recordType)
        row4 = VHRow([20],['id'],self.recordType)
        row5 = VHRow([18],['id'],self.recordType)
        row6 = VHRow([1],['id'],self.recordType)
        row7 = VHRow([8],['id'],self.recordType)
        row8 = VHRow([14],['id'],self.recordType)

        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.btree.addItem(row3)
        self.btree.addItem(row4)
        self.assertTrue(self.btree.isEspinhaDorsal())
        self.btree.addItem(row5)
        self.btree.addItem(row6)
        self.btree.addItem(row7)
        self.btree.addItem(row8)
        self.assertFalse(self.btree.isEspinhaDorsal())
        self.btree.criarEspinhaDorsal()
        self.assertTrue(self.btree.isEspinhaDorsal())

    def testCalcHeight(self):
        row1 = VHRow([10],['id'],self.recordType)
        row2 = VHRow([15],['id'],self.recordType)
        row3 = VHRow([17],['id'],self.recordType)
        row4 = VHRow([16],['id'],self.recordType)
        row5 = VHRow([20],['id'],self.recordType)
        row6 = VHRow([1],['id'],self.recordType)
        row7 = VHRow([8],['id'],self.recordType)

        self.btree.addItem(row1)
        self.assertEqual(self.btree.getHeight(),1)
        self.btree.addItem(row2)
        self.assertEqual(self.btree.getHeight(),2)
        self.btree.addItem(row3)
        self.assertEqual(self.btree.getHeight(),3)
        self.btree.addItem(row4)
        self.assertEqual(self.btree.getHeight(),4)
        self.btree.addItem(row5)
        self.assertEqual(self.btree.getHeight(),4)
        self.btree.addItem(row6)
        self.assertEqual(self.btree.getHeight(),4)
        self.btree.addItem(row7)
        self.assertEqual(self.btree.getHeight(),5)

    def testCriarAlturaMinima(self):
        row1 = VHRow([4],['id'],self.recordType)
        row2 = VHRow([6],['id'],self.recordType)
        row3 = VHRow([7],['id'],self.recordType)
        row4 = VHRow([9],['id'],self.recordType)
        row5 = VHRow([8],['id'],self.recordType)
        row6 = VHRow([1],['id'],self.recordType)
        row7 = VHRow([2],['id'],self.recordType)
        row8 = VHRow([5],['id'],self.recordType)

        self.btree.addItem(row1)
        self.btree.addItem(row2)
        self.btree.addItem(row3)
        self.btree.addItem(row4)
        self.btree.addItem(row5)
        self.btree.addItem(row6)
        self.btree.addItem(row7)

        self.btree = self.btree.criarAlturaMinima()
        self.assertEqual(self.btree.getHeight(),3)
        self.btree.addItem(row8)
        self.assertEqual(self.btree.getHeight(), 4)

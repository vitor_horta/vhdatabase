class Node:
    value = None
    key = None
    next = None
    downNode = None

    def __init__(self,key=None, value=None, next=None,downNode = None):
        self.key = key
        self.next = next
        self.value = value
        self.downNode = None

    def getValue(self):
        return self.value.getValue()

    def getKey(self,column = None):
        if not isinstance(self.value, str):return self.value.getKey(column)
        return self.key
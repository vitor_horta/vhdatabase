# VHDB #

VHDB, um IMDB
que permite diversos tipos de indexação além de dar suporte a chaves compostas, leituras de arquivos sql e consultas SQL.

### Uso básico ###

from vhdatabase.VHDatabase import VHDatabase

from vhdatabase.constants import *

vhdb = VHDatabase ()

vhdb.addTable ( tableName = 'pessoa' , columns =[ 'id' , 'nome' , 'cidade'],

primaryKey =[ 'id'] , indexType = INDEX_TYPE_LINKEDLIST)

vhdb.addRow ( 'pessoa' ,[ '1' , 'Vitor' , 'JF' ])

vhdb.addRow ( 'pessoa'  ,['2' , 'Joao', 'BH' ])

vitor = vhdb.getItem('pessoa' ,[ '1' ])

joao = vhdb.getItem('pessoa' ,[ '2' ])

print ( vitor )

print ( joao )

Resultado:

row ( id = '1' , nome = 'Vitor' , cidade = 'JF')

### Utilizando SQL ###

sql = "SELECT COUNT FROM pessoa "

results = vhdb.parseSql(sql)
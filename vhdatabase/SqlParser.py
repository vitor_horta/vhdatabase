import re

class SqlParser:
    _vhdb = None

    def __init__(self,vhdb):
        self._vhdb = vhdb

    '''
    Interpreta SQL INSERT DELETE e SELECT
    Se for um SELECT, delega para parseSelect
    '''
    def parseSql(self,sql):
        splittedSql = sql.split()
        if splittedSql[0] == "INSERT" and splittedSql[1] == "INTO":
            tableName = splittedSql[2]
            values = sql.split('(')[1].split(')')[0].split(',')
            self._vhdb.addRow(tableName,values)
            return
        if splittedSql[0] == "DELETE" and splittedSql[1] == "FROM":
            tableName = splittedSql[2]
            key = [splittedSql[3]]
            self._vhdb.removeRow(tableName,key)
            return
        return self.parseSelect(sql)

    '''
    Interpreta SQL SELECT
    Identifica qual condição e qual join foram passados, bem como se é para realizar apenas um COUNT
    '''
    def parseSelect(self,sql):
        splittedSql = sql.split()

        condition = self.parseCondition(sql)
        join = self.parseJoin(sql)
        count = self.parseCount(sql)
        if splittedSql[0] == "SELECT":
            tableName = re.search('FROM \s*(.+?)(;|\s$|\s|$)', sql).group(1)
            itens = self._vhdb.select(tableName,condition,join,count)
            self.printAsTable(itens)
            return itens

    '''
    Imprime resultados da query em formato de tabela
    '''
    def printAsTable(self,itens):
        if not itens: return
        fmt = ''
        for i in range(0,len(itens[0]._fields)):
            fmt += '{:^9} | '

        print(fmt.format(*itens[0]._fields))

        for item in itens:
            itemStr = ['None' if v is None else v for v in item]
            print(fmt.format(*itemStr))

    '''
    Identifica qual coluna e chave da condição passada
    EX: SELECT FROM data_src WHERE datasrc_id = D1066
    m[0] = datasrc_id
    m[1] = D1066
    '''
    def parseCondition(self,sql):
        m = re.search('WHERE (.+?)(;|\s$|INNER JOIN|LEFT JOIN|RIGHT JOIN|FULL JOIN|$)', sql)
        if m:
            m = m.group(1)
            m = m.split('=')
            m[0] = m[0].replace(' ', '')
            m[1] = m[1].replace(' ', '')
            return self._vhdb.conditionTuple(m[0],m[1])
        return None

    '''
    Identifica se deve ser realizado apenas um COUNT
    '''
    def parseCount(self,sql):
        m = re.search('SELECT \s*(COUNT) \s*(FROM)', sql)
        if m: return True
        return False

    '''
    Idenifica o tipo do join, as tabelas e as colunas
    EX: SELECT FROM data_src INNER JOIN datsrcln ON datasrc_id = datasrc_id
    type = INNER
    joinedTable = datsrcln
    column1 = datasrc_id
    column2 = datasrc_id
    '''
    def parseJoin(self,sql):
        m = re.search('JOIN (.+?)(;|\s$|$|WHERE)', sql)
        if m:
            m = m.group(1)
            joinedTable = m.split()[0]
            m = re.search('ON (.+?)(;|\s$|$)', m).group(1)

            m = m.split('=')
            column1 = m[0].replace(' ', '')
            column2 = m[1].replace(' ', '')
            type = re.search('\s*(INNER|LEFT|RIGHT|FULL)\s*(JOIN)', sql).group(1)
            return self._vhdb.joinTuple(joinedTable,column1,column2,type)
        return None
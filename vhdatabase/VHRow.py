from collections import namedtuple
class VHRow:
    PRIMARY_KEY_NOT_FOUND = 'This row has no primary key: '

    def __init__(self,value,primaryKeys,recordType = None):
        value = [str(v) for v in value]
        self._value = recordType(*value)
        self._recordType = recordType
        self._primaryKeys = primaryKeys

    def getValue(self):
        return self._value

    def getKey(self, column = None):
        if column != None: return self.getColumnKey(column)

        key = ''
        for pKey in self._primaryKeys:
            key += "#" + str(getattr(self._value,pKey))
        return key

    def getColumnKey(self,column):
        return str(getattr(self._value,column))

    def checkCondition(self,condition):
        if not condition or getattr(self._value, condition.field) == condition.key:
            return True
        return False

    def __repr__(self):
        try:
           return str(self._value)
        except Exception as e:
            return 'Não encontrado.'

    def __gt__(self, other):
        return self.getKey() > other.getKey()

    def __eq__(self, other):
        if hasattr(other, 'getKey'): return self.getKey() == other.getKey()
        return self.getKey() == other
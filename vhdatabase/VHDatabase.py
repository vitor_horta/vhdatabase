from vhdatabase.VHTable import VHTable
from vhdatabase.constants import *
from vhdatabase.SqlParser import SqlParser
from sqlreader.SqlReader import SqlReader
from collections import namedtuple
from timeit import default_timer as timer

'''
Decorator
Recebe uma função (getAllItens,getItensWhere, selectJoinedTable) e decide através do parametro count
se o retorno deve ser uma lista de itens ou o total de itens
'''
def returnItems(func):
    def getItems(*args,count = False):
        results = func(*args)
        if count:
            countNamedTuple = namedtuple('Count', 'count')
            c = countNamedTuple(len(results))
            return [c]
        return results
    return getItems

class VHDatabase:
    MSG_TABLE_EXISTS = 'This table already exists.'
    MSG_FIELDS_NOT_LIST = 'A table must have be a list with at least one field.'


    def __init__(self):
        self._tables = {}
        self.conditionTuple = namedtuple('Condition', ['field','key'])
        self.joinTuple = namedtuple('Join', ['table','column1','column2','type'])
        self._sqlParser = SqlParser(self)
        self._joins = {'LEFT': self.leftJoin, 'RIGHT': self.rightJoin,'INNER': self.innerJoin, 'FULL': self.fullJoin}


    '''
    Adiciona tabela e armazena em um dict
    O tipo de indexação pode ser definido pelo usuário. Por default será utilizada uma lista encadeada
    '''
    def addTable(self,tableName, columns, primaryKey = None,indexType = INDEX_TYPE_LINKEDLIST):
        if not isinstance(columns,list) or len(columns) <= 0:
            raise TypeError(self.MSG_FIELDS_NOT_LIST)

        if self.containsTable(tableName):
            raise NameError(self.MSG_TABLE_EXISTS)

        self._tables[tableName] = VHTable(tableName, columns, primaryKey, indexType)
        return True

    '''
    Localiza tabela no dict e delega adição de registro
    '''
    def addRow(self,tableName,row):
        if not row: return False
        table = self._tables[tableName]
        return table.addRow(row)

    '''
    Checa se o DB possui uma determinada tabela
    '''
    def containsTable(self,tableName):
        return (tableName in self._tables)

    '''
    Busca item concatenando as chaves primárias com simbolo #
    '''
    def getItem(self,tableName,keys,condition = None):
        table = self._tables[tableName]
        return table.getItem(keys,condition)

    '''
    Configura select
    Um select possui uma tabela alvo, uma condição opcional e um join opcional
    A condição possui uma coluna e uma chave
    O join possui a tabela do join, o campo da tabela 1, o campo da tabela 2 e o tipo do join
    O tipo do join pode ser LEFT, RIGHT ou INNER
    '''
    def select(self,tableName,condition,join, count = False):
        if isinstance(join,dict): join = self.makeJoinFromDict(join)
        if isinstance(condition, dict): condition = self.makeConditionFromDict(condition)
        return self.runSelect(tableName,condition,join, count)

    '''
    Executa o select
    Se não tiverem condição e join, realiza busca por todos os itens
    Se houver apenas condição, realiza uma busca que atenda a condição
    Se houver join, realiza um join. A operação de join aplica a condição (caso haja) em cada item
    '''
    def runSelect(self,tableName,condition = None, join = None, count = True):
        if not condition and not join: return self.getAllItens(tableName, count=count)
        if not join: return self.getItemWhere(tableName,condition.field,condition.key, count=count)
        return self.selectJoinedTable(tableName,join,condition, count=count)

    '''
    Realiza join. Ex: SELECT FROM T1 INNER JOIN T2 ON field = field
    Caso seja LEFT JOIN, inverte T1 com T2
    Busca todos os itens de t2 e procura os itens correspondentes em T1

    Ver decorator @returnItems
    '''
    @returnItems
    def selectJoinedTable(self, tableName,join, condition):
        return self._joins[join.type](tableName,join, condition)

    def innerJoin(self,tableName,join, condition):
        t1 = self._tables[tableName]
        t2 = self._tables[join.table]
        return t2.joinWith(t1, join, condition)

    def leftJoin(self,tableName,join, condition):
        t1 = self._tables[tableName]
        t2 = self._tables[join.table]
        return t1.joinWith(t2, join, condition)

    def rightJoin(self,tableName,join, condition):
        t1 = self._tables[tableName]
        t2 = self._tables[join.table]
        return t2.joinWith(t1, join, condition)

    def fullJoin(self,tableName,join, condition):
        rightJoin = self.joinTuple(join.table, join.column1, join.column2, "RIGHT")
        results1 = self.selectJoinedTable(tableName, rightJoin, condition)
        for row in self._tables[tableName]:
            key = getattr(row, join.column1)
            matched = self._tables[join.table].getItem([key])
            if not matched:
                results1.append(row)
        return results1
    '''
    Retorna todos os itens da tabela

    Ver decorator @returnItems
    '''
    @returnItems
    def getAllItens(self, tableName):
        table = self._tables[tableName]
        return table.getAllItens()


    '''
    Retorna todos os itens da tabela que possuem o valor 'key' na coluna 'column'

    Ver decorator @returnItems
    '''
    @returnItems
    def getItemWhere(self, tableName, column, key):
        table = self._tables[tableName]
        return table.getItemWhere(column, key)

    '''
    Cria namedtuple condition através de um dict
    '''
    def makeConditionFromDict(self,d):
        conditionField = list(d)[0]
        key = d[conditionField]
        return self.conditionTuple(conditionField,key)

    '''
    Cria namedtuple join através de um dict
    '''
    def makeJoinFromDict(self,d):
        joinedTable = list(d)[0]
        column = d[joinedTable]
        return self.joinTuple(joinedTable,column)

    def removeRow(self,tableName,key):
        return self._tables[tableName].removeRow(key)

    '''
    Lê e insere tabelas a partir de um arquivo no formato usda.sql
    '''
    def readTablesFromFile(self,filename,indexType = INDEX_TYPE_LINKEDLIST):
        reader = SqlReader()
        reader.openFile(filename)
        while True:
            tableName = reader.findCreateTable()
            if not tableName: break
            columns = reader.makeTable()
            self.addTable(tableName,columns,[],indexType)
        reader.closeFile()
        self.readPKeys(filename)

    '''
    Leitura de pkeys no arquivo usda.sql
    '''
    def readPKeys(self,filename):
        reader = SqlReader()
        reader.openFile(filename)
        reader.findPKeys(self.addPkey)
        reader.closeFile()

    '''
    Seta as chaves primárias da tabela recebida
    '''
    def addPkey(self,tableName,pkeys):
        table = self._tables[tableName]
        if pkeys is None:
            pkeys = [list(table._columns)[0]]
        else:
            pkeys = [x.strip(' ') for x in pkeys]
        table.setPrimaryKeys(pkeys)

    '''
    Lê e insere registros a partir de um arquivo no formato usda.sql
    '''
    def readRowsFromFile(self,filename):
        start = timer()

        reader = SqlReader()
        reader.openFile(filename)
        reader.findInsertions(self.addRow)
        reader.closeFile()
        end = timer()
        print("TEMPO DE LEITURA ROWS: " + str(end - start))

    '''
    Delega ordenação de todas as tabelas
    '''
    def sortTables(self):
        for key in self._tables:
            self._tables[key]._rows = self._tables[key].sort()

    def parseSql(self,sql):
        return self._sqlParser.parseSql(sql)

    def parseSelect(self,sql):
        return self._sqlParser.parseSelect(sql)

    def parseSelectWithCondition(self,splittedSql,condition):
        return self._sqlParser.parseSelectWithCondition(splittedSql,condition)

    def parseCondition(self,sql):
        return self._sqlParser.parseCondition(sql)

    def parseJoin(self,sql):
        return self._sqlParser.parseJoin(sql)

    def printItem(self,tableName,keys):
        item = self.getItem(tableName,keys)
        print(item)

    def __repr__(self):
        return "Tables: " + str(self._tables)
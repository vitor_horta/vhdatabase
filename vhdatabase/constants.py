from BinarySearchTree.BinarySearchTree import BinarySearchTree
from linkedList.LinkedList import LinkedList
from vhlist.VHList import VHList
from vhskiplist.vhskiplist.VHSkipList import VHSkipList
INDEX_TYPE_BSTREE = BinarySearchTree
INDEX_TYPE_LIST = VHList
INDEX_TYPE_LINKEDLIST = LinkedList
INDEX_TYPE_SKIPLIST = VHSkipList
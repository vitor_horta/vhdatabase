from vhdatabase.constants import *
from vhdatabase.VHRow import VHRow
from collections import namedtuple
from timeit import default_timer as timer

def getTime(func):
    def run(*args):
        print("Realizando " + func.__name__)
        start = timer()
        results = func(*args)

        end = timer()
        print("TEMPO DE JOIN: " + str(end - start))
        return results
    return run

class VHTable:
    PRIMARY_KEY_EXISTS = 'This primary key already exists: '
    INCORRECT_PRIMARY_KEY = 'The specified primary key does not exists in table columns: '

    def __init__(self,name, columns = [],primaryKeys = None,indexType = INDEX_TYPE_LINKEDLIST):
        self._name = str(name)
        self._columns = columns
        self._rows = indexType()
        self.clearColumnsName()
        self._recordType = namedtuple('row', columns)
        self._recordType.__new__.__defaults__ = (None,) * len(self._recordType._fields)
        self._primaryKeys = primaryKeys

    '''
    Checa se a chave passada é a chave inteira da tabela
    '''
    def isPkey(self,key):
        if self._primaryKeys[0] == key and len(self._primaryKeys) == 1: return True
        return False

    '''
    Checa se a chave passada é a primeira parte da chave primária da tabela
    '''
    def isFirstPkey(self,key):
        if self._primaryKeys[0] == key: return True
        return False

    def clearColumnsName(self,):
        for key,column in enumerate(self._columns):
            self._columns[key] = column.replace('"','').replace("'","").strip()

    def setPrimaryKeys(self,primaryKeys):
        self._primaryKeys = primaryKeys

    '''
    Cria VHRow com sua devida namedtuple e adiciona na tabela
    '''
    def addRow(self,row):
        vhRow = VHRow(row,self._primaryKeys,self._recordType)
        return self._rows.addItem(vhRow)

    '''
    Monta chave primária a partir de array de itens
    EX: makeKey([1,'abc']) => #1#abc
    '''
    def makeKey(self,keys):
        searchKey = ''
        for key in keys:
            searchKey = searchKey + '#' + str(key)
        return searchKey

    '''
    Monta chave primária a partir de array de itens e busca item pela chave primária
    Ver makeKey
    '''
    def getItem(self,keys,condition = None):
        searchKey = self.makeKey(keys)

        item = self._rows.getItem(searchKey,condition)
        if not item: return False
        return item

    '''
    Decide estratégia de join dos itens desta tabela 'self' com a tabela 'joinedTable'
    Se o join for feito na chave primária de joinedTable, é chamado o método 'joinByPkey'
    Caso contrário, é chamado o método "joinByNonPkey"
    '''
    def joinWith(self,joinedTable,join,condition):
        joinedColumns = joinedTable._columns
        jNamedTuple = namedtuple("jNamedTuple", self._columns + joinedColumns, rename=True)
        jNamedTuple.__new__.__defaults__ = (None,) * len(jNamedTuple._fields)
        return self.runJoin(jNamedTuple,joinedTable,join,condition)

    def runJoin(self,jNamedTuple,joinedTable,join,condition):
        if joinedTable.isPkey(join.column1):
            if self.isFirstPkey(join.column2): return self.mergeJoin(jNamedTuple, joinedTable, join, condition)
            return self.nestedJoinWithIndex(jNamedTuple,joinedTable,join,condition)

        if joinedTable.isFirstPkey(join.column1): return self.mergeJoinNonUnique(jNamedTuple,joinedTable,join,condition)
        return self.nestedJoinWithoutIndex(jNamedTuple,joinedTable,join,condition)


    @getTime
    def mergeJoinNonUnique(self,jNamedTuple,joinedTable,join,condition):
        returnedItens = []
        joinedTable.resetSmartSearch()

        for item in self:
            if self.checkCondition(item, condition):
                jItem = None
                jItems = joinedTable.getItemWhere(join.column1, getattr(item, join.column2), True)
                for jItem in jItems:
                    self.mergeJItens(returnedItens, jNamedTuple, item, jItem, join, condition)
                if not jItems and self.checkCondition(item, condition):
                    self.mergeJItens(returnedItens, jNamedTuple, item, jItem, join, condition)
        return returnedItens

    '''
    Realiza join dos itens desta tabela 'self' com a tabela 'joinedTable' em caso de chave priḿaria
    Se a chave for primeira parte da chave primaria em 'self', retorna seçf.mergeJoin
    Como a chave é inteiramente primária, para cada item pode haver apenas um correspondente em 'joinedTable'
    Logo, para cada item é feito uma busca em joinedTable pelo item correspondente
    Os itens correspondentes são armazenados no array 'returnedItens'
    '''

    @getTime
    def nestedJoinWithIndex(self,jNamedTuple,joinedTable,join,condition):
        returnedItens = []
        for item in self:
            jItem = joinedTable.getItem([getattr(item, join.column1)], condition)
            self.mergeJItens(returnedItens,jNamedTuple,item,jItem,join,condition)
        return returnedItens

    '''
    Realiza merge join entre t1 e t2 tal que a chave primária de t1 é primeira chave de t2
    Se rowT1 == rowT2, avança rowT2 pois podem haver outras rowsT2 correspondentes
    Se rowT1 < rowT2, avança rowT1 pois já acabaram as rowsR2 correspondentes 
    Se rowT1 > rowT2, avança rowT2 e termina
    '''

    @getTime
    def mergeJoin(self,jNamedTuple,joinedTable,join,condition):
        returnedItens = []
        t1 = iter(joinedTable)
        t2 = iter(self)
        row = next(t1)
        row2 = next(t2)
        try:
            while(True):
                if getattr(row,join.column1) == getattr(row2,join.column2):
                    self.mergeJItens(returnedItens,jNamedTuple,row,row2,join,condition)
                    row2 = next(t2)
                if getattr(row,join.column1) < getattr(row2,join.column2):
                    row = next(t1)
                elif getattr(row,join.column1) > getattr(row2,join.column2):
                    next(t2)
        except:
            pass

        return returnedItens

    '''
    Realiza join dos itens desta tabela 'self' com a tabela 'joinedTable' em caso de chave não primária
    Identifica se chave é a primeira parte de uma chave composta, neste caso é possível melhorar o algoritmo de join
    Como a chave não é inteiramente primária, para cada item pode haver um conjunto de joinedItens em joinedTable
    Logo, para cada item é feito uma busca em joinedTable pelo conjunto de itens correspondentes
    Os itens correspondentes são armazenados no array 'returnedItens'
    '''
    @getTime
    def nestedJoinWithoutIndex(self,jNamedTuple,joinedTable,join,condition):
        returnedItens = []

        for item in self:
            if self.checkCondition(item,condition):
                jItem = None
                jItems = joinedTable.getItemWhere(join.column1, getattr(item, join.column2))
                for jItem in jItems:
                    self.mergeJItens(returnedItens, jNamedTuple, item, jItem,join,condition)
                if not jItems and self.checkCondition(item,condition):
                    self.mergeJItens(returnedItens, jNamedTuple, item, jItem, join,condition)

        return returnedItens

    '''
    Realiza o merge entre o item e o joinedItem e adiciona no array de itens, que representa os itens a serem retornados
    Cria um novo namedtuple para representar a junção dos itens
    O merge é realizado apenas se a condição for satisfeita. Isto é feito para suportar JOIN com WHERE
    Se o join não for do tipo INNER, o item é adicionado mesmo que não haja um correspondente
    '''
    def mergeJItens(self,itens,jNamedTuple,item,jItem,join,condition):
        if jItem and self.checkCondition(jItem,condition):
            newItem = jNamedTuple(*(item + jItem))
            itens.append(newItem)
            return itens
        if join.type != "INNER":
            if self.checkCondition(item,condition):
                newItem = jNamedTuple(*item)
                itens.append(newItem)
                return itens

    '''
    Verifica se o item, que neste caso é um namedtuple, satisfaz a condição
    '''
    def checkCondition(self,item,condition):
        if not condition or getattr(item, condition.field) == condition.key:
            return True
        return False

    '''
    Retorna todos os itens desta tabela
    '''
    def getAllItens(self):
        return self._rows.getAllItens()

    '''
    Retorna todos os itens desta tabela tal que item.column == key
    Se column for a primeira parte da chave primária essa busca pode ser feita de forma mais rápida.
    '''
    def getItemWhere(self,column,key,isFirstPkey = None):
        itens = self._rows.getItemWhere(column,key,isFirstPkey)
        return itens

    '''
    Busca item pela chave primária e remove da tabela
    '''
    def removeRow(self,keys):
        searchKey = self.makeKey(keys)
        return self._rows.removeItem(searchKey)

    '''
    Ordena tabela
    '''
    def sort(self):
        return self._rows.sort()

    def __repr__(self):
        return self._name + " " + str(self._columns)

    def __str__(self):
        return str(self._rows)

    def __iter__(self):
        return iter(self._rows)

    def hasName(self,name):
        return self._name == name

    def resetSmartSearch(self):
        self._rows.resetSmartSearch()
from client.DBClient import DBClient
from client.SQLClient import SQLClient
from vhdatabase.VHDatabase import VHDatabase
from vhdatabase.constants import *
from BinarySearchTree.BinarySearchTree import BinarySearchTree
from BinarySearchTree.AvlNode import AvlNode
from collections import namedtuple
from vhdatabase.VHRow import VHRow
import sys
sys.setrecursionlimit(10000)

vhdb = VHDatabase()

client = DBClient(vhdb)
client.run()
sqlClient = SQLClient(vhdb)

from vhdatabase.VHDatabase import VHDatabase
from vhdatabase.constants import *

class SQLClient:
    _vhdb = None
    def __init__(self,vhdb):
        self._vhdb = vhdb

        self._vhdb.addTable('pessoa',['id','nome','cidade'],['id'],INDEX_TYPE_LINKEDLIST)
        self._vhdb.addTable(tableName='filho', columns=['id', 'idpessoa', 'filho'], primaryKey=['id'],
                      indexType=INDEX_TYPE_SKIPLIST)
        sql = "INSERT INTO pessoa (1,vitor,JF)"
        self._vhdb.parseSql(sql)
        sql = "INSERT INTO filho (1,1,fihovitor)"
        self._vhdb.parseSql(sql)
        sql = "INSERT INTO filho (20,2,filholeia)"
        self._vhdb.parseSql(sql)
        self.askSql()
    def askSql(self):
        try:
            while (True):
                sql = input('Digite a sql: ')
                self._vhdb.parseSql(sql)
        except Exception as e:
            print("Sql error, try again")
            print(str(e))
            self.askSql()


    def run(self):
        sql = input('Insira um sql: ')
        self._vhdb.parseSql(sql)
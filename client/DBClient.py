from vhdatabase.VHDatabase import VHDatabase
from vhdatabase.VHRow import VHRow
from vhdatabase.constants import *
from timeit import default_timer as timer

class DBClient:
    _vhdb = None

    def __init__(self,vhdb):
        self._vhdb = vhdb
        vhdb.addTable('skiplist',['id'],['id'],INDEX_TYPE_SKIPLIST)
        row = [str(3)]
        vhdb.addRow('skiplist',row)
        row = [str(3)]
        vhdb.addRow('skiplist',row)
        row = [str(2)]
        vhdb.addRow('skiplist', row)
        row = [str(4)]
        vhdb.addRow('skiplist', row)
        row = [str(0)]
        vhdb.addRow('skiplist', row)
        row = [str(60)]
        vhdb.addRow('skiplist', row)
        row = [str(59)]
        vhdb.addRow('skiplist', row)
        row = [str(8123)]
        vhdb.addRow('skiplist', row)
        # vhdb.printItem('skiplist',['1'])
        vhdb.printItem('skiplist',['3'])
        vhdb.printItem('skiplist',['3'])
        vhdb.printItem('skiplist',['4'])
        vhdb.printItem('skiplist',['0'])
        vhdb.printItem('skiplist',['60'])
        vhdb.printItem('skiplist',['59'])
        vhdb.printItem('skiplist',['8123'])

    def run(self):
        options = {1: self.testListContigua, 2: self.testLinkedList, 3: self.testBSTree, 4: self.testUsda, 6: self.testGetAllLinkedList,7: self.testGetAllList,8: self.testGetAllBSTree,9: self.testGetAllSkipList,10: self.testSkipList}
        initialMsg = "Escolha uma entre as opções: "
        menuMsg = "1: Teste em lista contígua (ordenada com busca binária)"
        menuMsg += "\n2: Teste em lista encadeada"
        menuMsg += "\n3: Teste em arvore binária de busca (àrvore cheia)"
        menuMsg += "\n4: Leitura do arquivo usda.sql"
        print(initialMsg)
        print(menuMsg)

        option = int(input('Digite a opção: '))
        options[option]()

    def executeWithTime(self,msg, action, *args):
        start = timer()
        action(*args)
        end = timer()
        totalTime = str(end - start)
        # print(msg + str(end - start))
        return totalTime

    def testList(self,tableName,indexType):
        self._vhdb.addTable(tableName,['id','name'],['id'],indexType)

        self.executeWithTime("Tempo total de inserção dos valores ",self.addRows,tableName)

        # self.executeWithTime("Tempo total de ordenação ",self._vhdb.sortTables)
        option = int(input('Digite um número de 1 a 99999 para realizar a busca: '))
        while not option is '-1' and not option is -1:
            self.executeWithTime("Tempo de busca: ",self._vhdb.printItem,tableName,[str(option)])
            option = int(input('Digite um número de 1 a 99999 para realizar a busca: '))

    def testListContigua(self):
        tableName = 'testlist'
        self.testList(tableName,INDEX_TYPE_LIST)

    def testSkipList(self):
        vhdb = VHDatabase()
        tableName = 'testskiplist'
        self.testList(tableName,INDEX_TYPE_SKIPLIST)

    def testLinkedList(self):
        tableName = 'testlinked'
        self.testList(tableName,INDEX_TYPE_LINKEDLIST)

    def testBSTree(self):
        tableName = 'testbstree'
        self.testList(tableName,INDEX_TYPE_BSTREE)

    def testUsda(self):
        filename = input('Informe o caminho do arquivo usda.sql: ')

        tables = {1: 'data_src', 2: 'datsrcln', 3: 'deriv_cd', 4: 'fd_group', 5: 'food_des',6: 'footnote',7: 'nut_data',8: 'nutr_def',9:'src_cd',10:'weight'}
        indexTypes = {1: INDEX_TYPE_LIST,2: INDEX_TYPE_LINKEDLIST, 3: INDEX_TYPE_BSTREE, 4: INDEX_TYPE_SKIPLIST}
        print('Escolha o método de indexação')
        menuMsg = "1: Lista contígua (ordenada com busca binária)"
        menuMsg += "\n2: Lista encadeada"
        menuMsg += "\n3: Arvore binária (com algoritmo de altura mínima)"
        menuMsg += "\n4: Skiplist"
        print(menuMsg)
        indexType = int(input('Digite a opção: '))
        indexType = indexTypes[indexType]
        print('Lendo arquivo...')
        self._vhdb.readTablesFromFile(filename,indexType)

        self._vhdb.readRowsFromFile(filename)
        # self.executeWithTime("Tempo total de ordenação (todas as tabelas): ", self._vhdb.sortTables)

        print('Escolha uma tabela para realizar buscas')
        menuMsg = "1: data_src"
        menuMsg += "\n2: datsrcln"
        menuMsg += "\n3: deriv_cd"
        menuMsg += "\n4: fd_group"
        menuMsg += "\n5: food_des"
        menuMsg += "\n6: footnote"
        menuMsg += "\n7: nut_data"
        menuMsg += "\n8: nutr_def"
        menuMsg += "\n9: src_cd"
        menuMsg += "\n10: weight"
        print(menuMsg)

    def addRows(self,tableName,totalRows=10000,interval = 10000):
        initialRows = totalRows - interval
        for i in range(initialRows,totalRows):
            row = [str(i),"row " + str(i)]
            self._vhdb.addRow(tableName,row)

    def testGetAllLinkedList(self):
        tableName = 'testlist'
        self._vhdb.addTable(tableName, ['id', 'name'], ['id'], INDEX_TYPE_LINKEDLIST)
        for totalRows in range(0,10000,100):
            self.addRows(tableName,totalRows)
            self.getWorstTime(tableName,totalRows)

    def testGetAllList(self):
        tableName = 'testlist'
        self._vhdb.addTable(tableName, ['id', 'name'], ['id'], INDEX_TYPE_LIST)
        for totalRows in range(100,10000,100):
            self.addRows(tableName,totalRows,100)
            self.getWorstTime(tableName,totalRows)
        for totalRows in range(10000,50000,1000):
            self.addRows(tableName,totalRows,1000)
            self.getWorstTime(tableName,totalRows)

    def testGetAllBSTree(self):
        tableName = 'testbstree'
        self._vhdb.addTable(tableName, ['id', 'name'], ['id'], INDEX_TYPE_BSTREE)
        for totalRows in range(100,1000,99):
            self.addRows(tableName,totalRows,99)
            # self._vhdb.sortTables()
            self.getWorstTime(tableName,totalRows)
        for totalRows in range(10000,50000,999):
            self.addRows(tableName,totalRows,999)
            # self._vhdb.sortTables()
            self.getWorstTime(tableName,totalRows)

    def testGetAllSkipList(self):
        tableName = 'testskiplist'
        self._vhdb.addTable(tableName, ['id', 'name'], ['id'], INDEX_TYPE_SKIPLIST)
        for totalRows in range(100,10000,100):
            self.addRows(tableName,totalRows,100)
            self.getWorstTime(tableName,totalRows)
        for totalRows in range(10000,50000,1000):
            self.addRows(tableName,totalRows,1000)
            self.getWorstTime(tableName,totalRows)

    def getAllRows(self,tableName):
        totalRows = 10000
        for i in range(1,totalRows):
            self.executeWithTime(str(i) + ' ',self._vhdb.getItem,tableName,[str(i)])

    def getWorstTime(self,tableName,totalRows):
        worstTime = -0.1
        for i in range(1, totalRows-1):
            timeElapsed = self.executeWithTime(str(i) + ' ', self._vhdb.getItem, tableName, [str(i)])
            if float(timeElapsed) > float(worstTime):
                worstTime = timeElapsed
        print(str(totalRows) + " " + str(worstTime))
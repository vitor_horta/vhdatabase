import math

class VHList:

    def __init__(self):
        self.__list = []

    def addItem(self,row):
        if len(self.__list) == 0:
            self.__list.append(row)
            return True

        position = self.binarySearch(row.getKey(),True)
        if not isinstance(position,int): return False
        self.__list.insert(position+1,row)
        return True

    def getItem(self,key,condition = None, returnIndex = False):
        return self.binarySearch(key,condition,returnIndex=returnIndex)

    def getItemWhere(self,column,key,isFirstPkey = None):
        return [item.getValue() for item in self.__list if item.getKey(column) == key]

    def getAllItens(self):
        return [item.getValue() for item in self.__list]

    def removeItem(self,key):
        index = self.getItem(key,returnIndex=True)
        if not type(index) == int: return False
        del self.__list[index]
        return True

    def resetSmartSearch(self):
        pass

    def binarySearch(self, key,condition = None,inserting = False,returnIndex = False):
        first = 0
        alist = self.__list
        last = len(alist) - 1
        found = False
        midpoint = 0
        while first <= last and not found:
            midpoint = (first + last) // 2
            if alist[midpoint].getKey() == key and alist[midpoint].checkCondition(condition):
                if returnIndex:
                    return midpoint
                return alist[midpoint].getValue()
            if key < alist[midpoint].getKey():
                last = midpoint - 1
            else:
                first = midpoint + 1

        if inserting:
            if key > alist[midpoint].getKey(): return midpoint
            return midpoint-1
        return False

    def sort(self):
        self.__list = self.mergeSort(self.__list)
        return self

    def mergeSort(self,v):
        if(len(v) == 1): return v
        mid = int(len(v) / 2)
        return self.merge(self.mergeSort(v[:mid]), self.mergeSort(v[mid:]))

    def merge(self,v1,v2):
        i = 0
        j = 0
        mergedArray = []

        while i+j < (len(v1)+len(v2)):
            if i >= len(v1):
                mergedArray.append(v2[j])
                j += 1
                continue
            if j >= len(v2):
                mergedArray.append(v1[i])
                i += 1
                continue

            if v1[i] < v2[j]:
                mergedArray.append(v1[i])
                i += 1
                continue
            mergedArray.append(v2[j])
            j += 1
        return mergedArray

    def __str__(self):
        return str(self.__list)

    def __len__(self):
        return len(self.__list)

    def __getitem__(self, key):
        return self.__list[key].getValue()
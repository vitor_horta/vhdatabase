import re
from timeit import default_timer as timer

class SqlReader:

    def __init__(self):
        self._f = None

    def openFile(self,filename):
        self._f = open(filename, 'r',encoding="ISO-8859-1")

    def closeFile(self):
        self._f.close()

    def findCopyInstruction(self):
        for line in self._f:
            line = line.split(' ')
            if line[0] == 'COPY': return line[1]

        return False

    def findInsertions(self,action):
        tableName = self.findCopyInstruction()
        while tableName:
            print("Realizando " + tableName)
            start = timer()
            for row in self._f:
                row = self.makeRow(row)
                if not row: break
                action(tableName,row)
            end = timer()
            print(str(tableName) + " " + str(end - start))
            tableName = self.findCopyInstruction()

    def findPKeys(self,action):
        for line in reversed(list(self._f)):
            if 'PRIMARY' in line:
                splitted = line.split()
                tableName = splitted[2][:-5]
                line = line.split('(')
                line = line[-1]
                line = line.split(')')
                line = line[0]
                line = line.split(',')
                action(tableName, line)
            if '\.' in line:
                return False

        return False

    def makeRow(self, row):
        row = [s.strip() for s in row.split('   ') if s]
        madeRow = []
        for partRow in row:
            r = re.split(r'\t+', partRow)
            for partR in r:
                if partR is not '': madeRow.append(partR)

        if madeRow[0] == '\.': return None
        madeRow[0] = madeRow[0].replace(" ", "")
        return madeRow

    def findCreateTable(self):
        for line in self._f:
            l = line.split(' ')
            if l[0] == 'CREATE' and l[1] == 'TABLE': return l[2]
            if "COPY" in line: return False
        return False

    def makeTable(self):
        columns = []
        for line in self._f:
            line = line.strip().split(" ")

            if line[0] == '(': continue
            if line[0] == ');': break
            columns.append(line[0])
        return columns
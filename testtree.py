from BinarySearchTree.BinarySearchTree import BinarySearchTree
from vhdatabase.VHRow import VHRow
from vhdatabase.VHDatabase import VHDatabase
from vhdatabase.constants import *
vhdb = VHDatabase()
tableName = 'testtree'
vhdb.addTable(tableName,['id','nome'],['id'],INDEX_TYPE_BSTREE)

vhdb.addRow(tableName,['2','test'])
vhdb.addRow(tableName,['1','test'])
vhdb.addRow(tableName,['3','test'])
# vhdb.printItem(tableName,['1'])
# vhdb.printItem(tableName,['2'])
# vhdb.printItem(tableName,['3'])
tree = vhdb._tables[tableName]._rows
print(tree.getHeight(tree._root))